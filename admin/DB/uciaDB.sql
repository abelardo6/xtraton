-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ucia
-- ------------------------------------------------------
-- Server version	5.6.25-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contactos`
--

DROP TABLE IF EXISTS `contactos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactos` (
  `id_contact` int(6) NOT NULL AUTO_INCREMENT,
  `nombre_con` varchar(100) NOT NULL,
  `telefono_con` int(11) NOT NULL,
  `email_con` varchar(100) NOT NULL,
  `mensaje_con` varchar(300) NOT NULL,
  `fecha_registro` date NOT NULL,
  PRIMARY KEY (`id_contact`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactos`
--

LOCK TABLES `contactos` WRITE;
/*!40000 ALTER TABLE `contactos` DISABLE KEYS */;
INSERT INTO `contactos` VALUES (1,'julio pereza',2147483647,'kjhkjh@kjhkjh.com','0','2015-04-16'),(2,'qwwqwqw',2147483647,'luis@yahoo.com','0','2015-06-15'),(3,'miguel p',2147483647,'miguel.pijuan@gmail.com','0','2015-09-23'),(4,'sdfadfasdf',2147483647,'mmmm@gmail.com','0','2015-09-23'),(5,'mpijuan',2147483647,'mmmm@gmail.com','0','2015-09-23'),(6,'asdfasfasdf',2147483647,'mmmm@gmail.com','0','2015-09-23'),(7,'asdfasfasdf',2147483647,'mmmm@gmail.com','0','2015-09-23'),(8,'asdfasfasdf',2147483647,'mmmm@gmail.com','0','2015-09-23'),(9,'asdfasfasdf',2147483647,'mmmm@gmail.com','0','2015-09-23'),(10,'cliente 1',2147483647,'ejemplo@correo.com','0','2015-09-26'),(11,'Pedro Perez',2147483647,'aaxxcc@any.com','0','2015-09-26'),(12,'Pedro Perez 1',2147483647,'aaxxcc@any1.com','Prueba de Mensaje 11','2015-09-26'),(13,'Pedro Perez 2',2147483647,'aaxxcc@any2.com','Prueba de Mensaje 22','2015-09-26'),(14,'Pedro Perez 3',2147483647,'aaxxcc@any3.com','Prueba de Mensaje 33','2015-09-26');
/*!40000 ALTER TABLE `contactos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_entradas`
--

DROP TABLE IF EXISTS `detalle_entradas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_entradas` (
  `cod_prod` varchar(13) NOT NULL,
  `id_entrada` int(6) NOT NULL,
  `cantidad_desp` int(7) NOT NULL COMMENT 'cantidad que se despacho'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_entradas`
--

LOCK TABLES `detalle_entradas` WRITE;
/*!40000 ALTER TABLE `detalle_entradas` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_entradas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_sal_equipos`
--

DROP TABLE IF EXISTS `detalle_sal_equipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_sal_equipos` (
  `id_salida_e` int(6) NOT NULL,
  `cod_e` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_sal_equipos`
--

LOCK TABLES `detalle_sal_equipos` WRITE;
/*!40000 ALTER TABLE `detalle_sal_equipos` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_sal_equipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_salidas`
--

DROP TABLE IF EXISTS `detalle_salidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_salidas` (
  `id_salida` int(6) NOT NULL,
  `cod_prod` varchar(13) NOT NULL,
  `cantidad_prod` int(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_salidas`
--

LOCK TABLES `detalle_salidas` WRITE;
/*!40000 ALTER TABLE `detalle_salidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_salidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entradas`
--

DROP TABLE IF EXISTS `entradas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entradas` (
  `id_entrada` int(6) NOT NULL AUTO_INCREMENT COMMENT 'numero referencia',
  `fecha` datetime NOT NULL COMMENT 'fecha registro de entrada al almacen',
  `cedula` int(12) NOT NULL COMMENT 'persona que recibe y firma la requisicion',
  `procedencia` varchar(100) DEFAULT NULL COMMENT 'de donde vienen los equipos o mteriales',
  `status` int(1) DEFAULT NULL COMMENT 'estado de la operacion',
  PRIMARY KEY (`id_entrada`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entradas`
--

LOCK TABLES `entradas` WRITE;
/*!40000 ALTER TABLE `entradas` DISABLE KEYS */;
/*!40000 ALTER TABLE `entradas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipos`
--

DROP TABLE IF EXISTS `equipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipos` (
  `cod_e` varchar(50) NOT NULL COMMENT 'serial de equipo numero de bien nacional',
  `nomb_e` varchar(100) NOT NULL COMMENT 'nombre de equipo',
  `img` varchar(300) NOT NULL COMMENT 'imagen del producto',
  `status_e` int(1) NOT NULL COMMENT 'estado del equipo.',
  `fecha_registro` date NOT NULL COMMENT 'fecha de registro en el sistema',
  `fecha_actualiza` date NOT NULL COMMENT 'fecha en la que se hizo alguna modificacion al registro',
  `observacion` varchar(140) DEFAULT NULL COMMENT 'acotaciones de la operacion de registro',
  `tipo_bn` varchar(60) DEFAULT NULL COMMENT 'tipo de bien nacional',
  `modelo` varchar(60) DEFAULT NULL COMMENT 'modelo del equipo segun el tipo de bien nacional',
  `marca` varchar(50) DEFAULT NULL COMMENT 'marca del equipo o bien nacional',
  PRIMARY KEY (`cod_e`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipos`
--

LOCK TABLES `equipos` WRITE;
/*!40000 ALTER TABLE `equipos` DISABLE KEYS */;
INSERT INTO `equipos` VALUES ('523423423412554','CORTADOR','img_equipos/email.png',2,'2015-04-27','2015-05-24','SOLO BASE 2','muebles','CUEGH45J','CASTINII'),('5234234234AS','CAMA MEDICA','img_equipos/CAMA_MEDICA.jpg',2,'2015-05-01','2015-07-27','TODA','','ASDA21','SEARHS'),('5234234234ASDASD','QUIRURGICAS','img_equipos/CATETERISMO_1.jpg',1,'2015-05-01','2015-06-13','JUEGO COMPLETO','','ASDASDWQE','CAPTAIN'),('12313SADA','RESONANCIA','img_equipos/BOMBA_1.jpg',1,'2015-05-03','2015-06-13','TOTAL A PARTS','','AK123122','RESONAR'),('5234234234qwe','TENSIOMETRO','img_equipos/TENSIOMETRO_1.jpg',1,'2015-05-01','2015-06-13','UNIDAD COMPLETA','','QE1212','searhcs'),('5234234234222333','camases','img_equipos/ndice.jpeg',2,'2015-04-22','2015-06-13','nada','muebles','ACMDJS1','ACMET'),('523423423444','Masajeador','img_equipos/TOMOGRAFO_1.jpg',2,'2015-06-04','2015-06-13','Es caja\n\n','','ACMDJS1444','SERIAL');
/*!40000 ALTER TABLE `equipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticias` (
  `id_noticia` int(7) NOT NULL AUTO_INCREMENT COMMENT 'numero de noticia',
  `cedula` int(12) NOT NULL COMMENT 'cedula de quien registra y redacta la noticia',
  `titulo` varchar(240) NOT NULL COMMENT 'titulo de la noticia',
  `num_pub` int(1) NOT NULL COMMENT '1: modelo 1 foto superior izquierda, 2: modelo 2 fotos, 3: modelo 4 fotos',
  `status` int(1) NOT NULL COMMENT 'status noticia 1: pendiente aprobacion, 2: aprobada',
  `fecha_redaccion` date NOT NULL COMMENT 'fecha en que se redacto la noticia',
  `fecha_actualizacion` date NOT NULL COMMENT 'ultima fecha de actualizacion',
  `fecha_publicacion` date NOT NULL COMMENT 'fecha de publicacion, sirve para programar noticias futuras',
  `img_1` varchar(200) DEFAULT NULL COMMENT 'imagen 1',
  `img_2` varchar(200) DEFAULT NULL COMMENT 'imagen 2',
  `img_3` varchar(200) DEFAULT NULL COMMENT 'imagen 3',
  `img_4` varchar(200) DEFAULT NULL COMMENT 'imagen 4',
  `texto_1` varchar(1500) DEFAULT NULL COMMENT 'texto 1',
  `texto_2` varchar(1500) DEFAULT NULL COMMENT 'texto 2',
  PRIMARY KEY (`id_noticia`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
/*!40000 ALTER TABLE `noticias` DISABLE KEYS */;
INSERT INTO `noticias` VALUES (14,7264039,'Mi Venezuela',2,1,'2015-05-03','2015-05-03','2015-05-10','img_noticias/maduro_13A.jpg','img_noticias/descarga_1_2.jpg','','','Durante un acto cívico-militar en Fuerte Tiuna, el presidente Nicolás Maduro ordenó investigar a los empresarios que importan ?dos productos que no se hacen en Venezuela? y dijo que, si era necesario, ?vamos a detenerlos y entregarlos a la Fiscalía porque estoy seguro que están detrás del plan de sabotear a nuestro pueblo?.\n\n\n\nEn cadena nacional de radio y televisión, el presidente dijo: ?Hoy me dijeron que en la fase actual de la guerra económica, tipo Chile, hay unos productos que ellos pusieron a ','Maduro advirtió: ?Yo le dije al vicepresidente ejecutivo lo mismo que le dije a otros vicepresidentes de gobierno: sin demora, sin falta, asas citen a los empresarios que se les entregaron los dólares para la importación de esos dos productos que no se hacen en Venezuela, muy sensibles a la vida adulta. Los citan, los investigan, revísenles las cuentas, vayan a los almacenes y, si es necesario, vamos a '),(21,7264039,'Patria',1,1,'2015-05-03','2015-05-03','2015-05-02','img_noticias/maduro_5_revoluciones_telesur_4_2.jpg','','','','Caracas, 16 Ene. AVN.- El vicepresidente Ejecutivo de la República, Jorge Arreaza, llamó a los abogados de la Universidad Bolivariana de Venezuela (UBV) a profundizar la justicia social impulsada por la Revolución Bolivariana.\n\n\n\n\"La unidad de nuestra revolución está intacta y está más fortalecida que nunca. En cuanto más nos ataquen, más nos vamos a unir y necesitamos esa nueva ética para activar la justicia social, para activar la Misión Justicia Socialista\", expresó Arreaza, durante un contacto con Venezolana de Televisión.\n\n\n\nAsimismo, exhortó a los abogados de la República a sumarse a las Bases de Misiones Socialistas.\n\n\n\n\"Allí están las Bases de Misiones, ahí está un pueblo que necesita justicia y para eso están ustedes, para interpretar las leyes, para ayudarlos. En las bases cuentan con todo, con las misiones Alimentación, Sucre, Robinson, entre otras, y lo que les hace falta es la Misión Justicia Socialista\", enfatizó.\n\n		\n\n   <h2><br class=\"spacer\" />\n\n   </h2>\n\n  </div>','\n\n\n\n'),(39,7264039,'Patria',1,1,'2015-07-12','2015-07-12','2015-05-15','img_noticias/descarga_2_1.jpg','','','','En toda organización, institución o empresa, la administración en base a los 5 principios fundamentales de la misma: (Planificación, Organización, Ejecución, Supervisión y Evaluación), es clave para el buen desarrollo de toda actividad. Esto nos lleva a visualizar la necesidad de una buena administración en la instalación de cada Escuela Bíblica.','En toda organización, institución o empresa, la administración en base a los 5 principios fundamentales de la misma: (Planificación, Organización, Ejecución, Supervisión y Evaluación), es clave para el buen desarrollo de toda actividad. Esto nos lleva a visualizar la necesidad de una buena administración en la instalación de cada Escuela Bíblica.'),(40,7264039,'Patria 2',2,1,'2015-07-28','2015-07-28','2015-07-27','img_noticias/BOMBA.jpg','img_noticias/CAMA_MEDICA.jpg','','','Cree un prospecto llamativo\n\nPara sustituir el texto de un marcador de posición (como este), haga clic en el mismo y empiece a escribir. \n\n?	Se han utilizado estilos para dar formato al texto de esta plantilla. Consulte la galería Estilos de la pestaña Inicio para aplicar un estilo con un simple clic.\n\n?	Para sustituir una foto, haga clic con el botón derecho en la misma y seleccione Cambiar imagen.\n\n?	Para probar otros aspectos para este prospecto, consulte las galerías Temas, Fuentes y Colores de la pestaña Inicio.\n\n?	Si le gusta el aspecto profesional de esta plantilla, use las plantillas de folleto y postal correspondientes para lograr un aspecto profesional y de marca en tan solo unos minutos.\n\n','Cree un prospecto llamativo\n\nPara sustituir el texto de un marcador de posición (como este), haga clic en el mismo y empiece a escribir. \n\n?	Se han utilizado estilos para dar formato al texto de esta plantilla. Consulte la galería Estilos de la pestaña Inicio para aplicar un estilo con un simple clic.\n\n?	Para sustituir una foto, haga clic con el botón derecho en la misma y seleccione Cambiar imagen.\n\n?	Para probar otros aspectos para este prospecto, consulte las galerías Temas, Fuentes y Colores de la pestaña Inicio.\n\n?	Si le gusta el aspecto profesional de esta plantilla, use las plantillas de folleto y postal correspondientes para lograr un aspecto profesional y de marca en tan solo unos minutos.\n\n');
/*!40000 ALTER TABLE `noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `cod_prod` varchar(13) NOT NULL COMMENT 'Este campo aplica para producto y equipo, en equipo es como el bien nacional',
  `nomb_prod` varchar(100) NOT NULL COMMENT 'nombre de producto',
  `cantidad` int(5) DEFAULT NULL COMMENT 'cantidad de equipos y/o productos',
  `status_p` int(1) NOT NULL COMMENT 'estado del producto o equipo.',
  `fecha_caducidad` date NOT NULL,
  `fecha_registro` date NOT NULL COMMENT 'fecha de registro en el sistema',
  `fecha_actualiza` date NOT NULL COMMENT 'fecha en la que se hizo alguna modificacion al registro',
  `observacion` varchar(140) DEFAULT NULL COMMENT 'acotaciones de la operacion de registro',
  PRIMARY KEY (`cod_prod`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES ('preeee','AGUA DESTILADA AL 200%',563614,1,'2015-07-13','2015-05-01','2015-06-02','POR DOCENAS '),('C2345','AGUJAS DE SUTURA CU',1687792,1,'2015-07-31','2015-04-27','2015-06-02',''),('P420','Agujas de suturas',4611322,2,'2015-05-31','2015-04-22','2015-05-03','En cajas de 500 unidades cada una'),('T5443','AGUA DESTILADA AL 200%',1692512,1,'2015-06-17','2015-04-27','2015-04-27','POR UNIDADAES');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recuperaciones`
--

DROP TABLE IF EXISTS `recuperaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recuperaciones` (
  `id_recuperacion` int(5) NOT NULL AUTO_INCREMENT COMMENT 'numero de recuperacion',
  `fecha_solicitud` datetime NOT NULL COMMENT 'fecha en que se hace la solcitud',
  `fecha_actualiza` datetime NOT NULL COMMENT 'fecha en que se hace el proceso y se recupera la informacion',
  `cedula` int(12) NOT NULL COMMENT 'cedula del usuario que solicita la recuperacion',
  `codigo` varchar(10) NOT NULL COMMENT 'codigo aleatorio de validacion',
  `status_recupera` int(1) NOT NULL COMMENT 'status de la validacion de clave',
  PRIMARY KEY (`id_recuperacion`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recuperaciones`
--

LOCK TABLES `recuperaciones` WRITE;
/*!40000 ALTER TABLE `recuperaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `recuperaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reparaciones`
--

DROP TABLE IF EXISTS `reparaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reparaciones` (
  `id_reparacion` int(7) NOT NULL AUTO_INCREMENT COMMENT 'numero de reparacion',
  `cod_e` varchar(50) NOT NULL COMMENT 'serial de equipo numero de bien nacional',
  `cedula` int(12) NOT NULL COMMENT 'cedula del administrador que registra los datos',
  `status_r` int(1) NOT NULL COMMENT '1: programada, 2: realizada, 3: eliminada',
  `fecha_prog` date NOT NULL COMMENT 'fecha de programacion',
  `fecha_actualiza` date NOT NULL COMMENT 'fecha de realizacion del mantenimiento o reparacion',
  `tipo_reparacion` int(1) DEFAULT NULL COMMENT '1: Preventivo, 2: Correctivo',
  `descripcion` varchar(240) DEFAULT NULL COMMENT 'descripcion de la reparacion',
  PRIMARY KEY (`id_reparacion`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reparaciones`
--

LOCK TABLES `reparaciones` WRITE;
/*!40000 ALTER TABLE `reparaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `reparaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salidas`
--

DROP TABLE IF EXISTS `salidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salidas` (
  `id_salida` int(6) NOT NULL AUTO_INCREMENT COMMENT 'numero de referencia',
  `fecha_salida` date NOT NULL COMMENT 'fecha de registro del pedido del usuario',
  `cedula` int(12) NOT NULL COMMENT 'cedula del usuario que pide el material',
  `cedula_entrega` int(13) DEFAULT NULL COMMENT 'cedula de quien hace la entrega del material',
  `fecha_entrega` date DEFAULT NULL COMMENT 'fecha de entrega del material',
  `status` int(1) NOT NULL COMMENT '1= pendiente, 2=entregado, 3=cancelado',
  PRIMARY KEY (`id_salida`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salidas`
--

LOCK TABLES `salidas` WRITE;
/*!40000 ALTER TABLE `salidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `salidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salidas_e`
--

DROP TABLE IF EXISTS `salidas_e`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salidas_e` (
  `id_salida_e` int(6) NOT NULL AUTO_INCREMENT COMMENT 'numero de referencia',
  `fecha_salida` date NOT NULL COMMENT 'fecha de registro del pedido del usuario',
  `cedula` int(12) NOT NULL COMMENT 'cedula del usuario que pide el material',
  `depart_prestamo` varchar(80) NOT NULL COMMENT 'departamento al que presta servicios la persona que retira el equipo',
  `retira` varchar(50) NOT NULL COMMENT 'nombre y apellido de quien retira el equipo',
  `ced_retira` int(12) NOT NULL COMMENT 'cedula de quien retira el equipo',
  `cedula_entrega` int(13) DEFAULT NULL COMMENT 'cedula de quien hace la entrega del material',
  `fecha_entrega` date DEFAULT NULL COMMENT 'fecha de entrega del material',
  `status` int(1) NOT NULL COMMENT '1= pendiente, 2=entregado, 3=cancelado',
  `observaciones` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id_salida_e`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salidas_e`
--

LOCK TABLES `salidas_e` WRITE;
/*!40000 ALTER TABLE `salidas_e` DISABLE KEYS */;
/*!40000 ALTER TABLE `salidas_e` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `cedula` int(12) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellido` varchar(35) NOT NULL,
  `telefono` bigint(16) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `direccion` varchar(240) DEFAULT NULL,
  `fecha_registro` date NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(15) NOT NULL,
  `privilegio` int(1) NOT NULL COMMENT 'administrador =1, usuario= 2',
  `status` varchar(1) NOT NULL,
  `cargo` varchar(100) NOT NULL COMMENT 'cargo del usuario dentro del hospital',
  `departamento` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`cedula`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (72640321,'Joseeee','Ramon',4241624517,'jrsp@yahoo.com','Maracay 11','2015-05-03','jose','123',2,'1','licenciado','UCI'),(7264039,'Carlos','Silva',1624516,'silvagca@yahoo.com','Petare','2015-04-27','carlos','123',1,'1','tsu',''),(141414,'Elias','Perdomo',41444411115,'kjhkjh@kjhkjh.com','cagua','2015-05-11','dani','123',1,'1','tsu',''),(2213345,'SANCHO','SANCHEZ',1214567899,'sancho@yahoo.com','barinas','2015-06-15','sancho','123',1,'1','licenciado',''),(45782000,'panrcacio','sinforoso',4125463695,'pancarsio@hotmail.com','La atlantida','2015-07-28','pancra','123',2,'1','tsu',''),(12345678,'Juan Jose','Perez Cruz',4242456952,'mmmm@gmail.com','Av. Caracas Juan Perez','2015-09-23','m1m2m3','m1m2m3',1,'1','tsu','TRAUMATOLOGIA');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_autoriza`
--

DROP TABLE IF EXISTS `usuarios_autoriza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_autoriza` (
  `cedula` int(12) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellido` varchar(35) NOT NULL,
  `telefono` bigint(16) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `direccion` varchar(240) DEFAULT NULL,
  `fecha_registro` date NOT NULL,
  `cargo` varchar(100) NOT NULL COMMENT 'cargo del usuario dentro del hospital',
  `departamento` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`cedula`),
  UNIQUE KEY `cedula` (`cedula`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_autoriza`
--

LOCK TABLES `usuarios_autoriza` WRITE;
/*!40000 ALTER TABLE `usuarios_autoriza` DISABLE KEYS */;
INSERT INTO `usuarios_autoriza` VALUES (22331144,'JUAN','PEREZ',4242456952,'mmmz@gmail.com','Prueba','2015-09-26','aux','UCI'),(2213233,'JOAN','STIX',4242456952,'ejjj@dmc.com','Prueba','2015-09-26','tsu','UCI');
/*!40000 ALTER TABLE `usuarios_autoriza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_salida_lista`
--

DROP TABLE IF EXISTS `v_salida_lista`;
/*!50001 DROP VIEW IF EXISTS `v_salida_lista`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_salida_lista` AS SELECT 
 1 AS `id_salida`,
 1 AS `status`,
 1 AS `fecha_entrega`,
 1 AS `cedula`,
 1 AS `nombre`,
 1 AS `apellido`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_salida_prod`
--

DROP TABLE IF EXISTS `v_salida_prod`;
/*!50001 DROP VIEW IF EXISTS `v_salida_prod`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_salida_prod` AS SELECT 
 1 AS `id_salida`,
 1 AS `fecha_entrega`,
 1 AS `status`,
 1 AS `cod_prod`,
 1 AS `nomb_prod`,
 1 AS `cantidad_prod`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'ucia'
--

--
-- Dumping routines for database 'ucia'
--

--
-- Final view structure for view `v_salida_lista`
--

/*!50001 DROP VIEW IF EXISTS `v_salida_lista`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `v_salida_lista` AS select `a`.`id_salida` AS `id_salida`,`a`.`status` AS `status`,`a`.`fecha_entrega` AS `fecha_entrega`,`a`.`cedula` AS `cedula`,`b`.`nombre` AS `nombre`,`b`.`apellido` AS `apellido` from (`salidas` `a` join `usuarios` `b` on((`a`.`cedula` = `b`.`cedula`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_salida_prod`
--

/*!50001 DROP VIEW IF EXISTS `v_salida_prod`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `v_salida_prod` AS select `a`.`id_salida` AS `id_salida`,`b`.`fecha_entrega` AS `fecha_entrega`,`b`.`status` AS `status`,`a`.`cod_prod` AS `cod_prod`,`c`.`nomb_prod` AS `nomb_prod`,`a`.`cantidad_prod` AS `cantidad_prod` from ((`detalle_salidas` `a` left join `salidas` `b` on((`a`.`id_salida` = `b`.`id_salida`))) left join `productos` `c` on((`a`.`cod_prod` = `c`.`cod_prod`))) order by `a`.`cod_prod` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-13 22:33:54
