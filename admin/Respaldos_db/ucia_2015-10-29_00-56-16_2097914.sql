DROP DATABASE IF EXISTS ucia;
CREATE DATABASE ucia;
USE ucia;


CREATE TABLE `contactos` (
  `id_contact` int(6) NOT NULL AUTO_INCREMENT,
  `nombre_con` varchar(100) NOT NULL,
  `telefono_con` int(11) NOT NULL,
  `email_con` varchar(100) NOT NULL,
  `mensaje_con` varchar(300) NOT NULL,
  `fecha_registro` date NOT NULL,
  PRIMARY KEY (`id_contact`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;


INSERT INTO contactos VALUES
("1","julio pereza","2147483647","kjhkjh@kjhkjh.com","0","2015-04-16"),
("2","qwwqwqw","2147483647","luis@yahoo.com","0","2015-06-15"),
("3","miguel p","2147483647","miguel.pijuan@gmail.com","0","2015-09-23"),
("4","sdfadfasdf","2147483647","mmmm@gmail.com","0","2015-09-23"),
("5","mpijuan","2147483647","mmmm@gmail.com","0","2015-09-23"),
("6","asdfasfasdf","2147483647","mmmm@gmail.com","0","2015-09-23"),
("7","asdfasfasdf","2147483647","mmmm@gmail.com","0","2015-09-23"),
("8","asdfasfasdf","2147483647","mmmm@gmail.com","0","2015-09-23"),
("9","asdfasfasdf","2147483647","mmmm@gmail.com","0","2015-09-23"),
("10","cliente 1","2147483647","ejemplo@correo.com","0","2015-09-26"),
("11","Pedro Perez","2147483647","aaxxcc@any.com","0","2015-09-26"),
("12","Pedro Perez 1","2147483647","aaxxcc@any1.com","Prueba de Mensaje 11","2015-09-26"),
("13","Pedro Perez 2","2147483647","aaxxcc@any2.com","Prueba de Mensaje 22","2015-09-26"),
("14","Pedro Perez 3","2147483647","aaxxcc@any3.com","Prueba de Mensaje 33","2015-09-26"),
("15","asdad","2147483647","ramonjrsp@yahoo.com","hols","2015-10-25"),
("16","qweqwe","2147483647","selestino@yahoo.com","hola","2015-10-25"),
("17","CARLOS SILVA","2147483647","silvagca@yahoo.com","loooo","2015-10-25");




CREATE TABLE `detalle_entradas` (
  `cod_prod` varchar(13) NOT NULL,
  `id_entrada` int(6) NOT NULL,
  `cantidad_desp` int(7) NOT NULL COMMENT 'cantidad que se despacho'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;






CREATE TABLE `detalle_sal_equipos` (
  `id_salida_e` int(6) NOT NULL,
  `cod_e` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;






CREATE TABLE `detalle_salidas` (
  `id_salida` int(6) NOT NULL,
  `cod_prod` varchar(13) NOT NULL,
  `cantidad_prod` int(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;






CREATE TABLE `entradas` (
  `id_entrada` int(6) NOT NULL AUTO_INCREMENT COMMENT 'numero referencia',
  `fecha` datetime NOT NULL COMMENT 'fecha registro de entrada al almacen',
  `cedula` int(12) NOT NULL COMMENT 'persona que recibe y firma la requisicion',
  `procedencia` varchar(100) DEFAULT NULL COMMENT 'de donde vienen los equipos o mteriales',
  `status` int(1) DEFAULT NULL COMMENT 'estado de la operacion',
  PRIMARY KEY (`id_entrada`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;






CREATE TABLE `equipos` (
  `cod_e` varchar(50) NOT NULL COMMENT 'serial de equipo numero de bien nacional',
  `nomb_e` varchar(100) NOT NULL COMMENT 'nombre de equipo',
  `img` varchar(300) NOT NULL COMMENT 'imagen del producto',
  `status_e` int(1) NOT NULL COMMENT 'estado del equipo.',
  `fecha_registro` date NOT NULL COMMENT 'fecha de registro en el sistema',
  `fecha_actualiza` date NOT NULL COMMENT 'fecha en la que se hizo alguna modificacion al registro',
  `observacion` varchar(140) DEFAULT NULL COMMENT 'acotaciones de la operacion de registro',
  `tipo_bn` varchar(60) DEFAULT NULL COMMENT 'tipo de bien nacional',
  `modelo` varchar(60) DEFAULT NULL COMMENT 'modelo del equipo segun el tipo de bien nacional',
  `marca` varchar(50) DEFAULT NULL COMMENT 'marca del equipo o bien nacional',
  PRIMARY KEY (`cod_e`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


INSERT INTO equipos VALUES
("523423423412554","CORTADOR","img_equipos/email.png","2","2015-04-27","2015-05-24","SOLO BASE 2","muebles","CUEGH45J","CASTINII"),
("5234234234AS","CAMA MEDICA","img_equipos/CAMA_MEDICA.jpg","2","2015-05-01","2015-07-27","TODA","","ASDA21","SEARHS"),
("5234234234ASDASD","QUIRURGICAS","img_equipos/CATETERISMO_1.jpg","2","2015-05-01","2015-06-13","JUEGO COMPLETO","","ASDASDWQE","CAPTAIN"),
("12313SADA","RESONANCIA","img_equipos/BOMBA_1.jpg","1","2015-05-03","2015-06-13","TOTAL A PARTS","","AK123122","RESONAR"),
("5234234234qwe","TENSIOMETRO","img_equipos/TENSIOMETRO_1.jpg","1","2015-05-01","2015-06-13","UNIDAD COMPLETA","","QE1212","searhcs");




CREATE TABLE `noticias` (
  `id_noticia` int(7) NOT NULL AUTO_INCREMENT COMMENT 'numero de noticia',
  `cedula` int(12) NOT NULL COMMENT 'cedula de quien registra y redacta la noticia',
  `titulo` varchar(240) NOT NULL COMMENT 'titulo de la noticia',
  `num_pub` int(1) NOT NULL COMMENT '1: modelo 1 foto superior izquierda, 2: modelo 2 fotos, 3: modelo 4 fotos',
  `status` int(1) NOT NULL COMMENT 'status noticia 1: pendiente aprobacion, 2: aprobada',
  `fecha_redaccion` date NOT NULL COMMENT 'fecha en que se redacto la noticia',
  `fecha_actualizacion` date NOT NULL COMMENT 'ultima fecha de actualizacion',
  `fecha_publicacion` date NOT NULL COMMENT 'fecha de publicacion, sirve para programar noticias futuras',
  `img_1` varchar(200) DEFAULT NULL COMMENT 'imagen 1',
  `img_2` varchar(200) DEFAULT NULL COMMENT 'imagen 2',
  `img_3` varchar(200) DEFAULT NULL COMMENT 'imagen 3',
  `img_4` varchar(200) DEFAULT NULL COMMENT 'imagen 4',
  `texto_1` varchar(1500) DEFAULT NULL COMMENT 'texto 1',
  `texto_2` varchar(1500) DEFAULT NULL COMMENT 'texto 2',
  PRIMARY KEY (`id_noticia`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;


INSERT INTO noticias VALUES
("41","7264039","TOMOGRAFO","1","1","2015-10-28","2015-10-28","2015-10-28","img_noticias/TOMOGRAFO.jpg","","","","TomografÃ­a es el procesamiento de imÃ¡genes por secciones. Un aparato usado en tomografÃ­a es llamado tomÃ³grafo, mientras que la imagen producida es un tomograma. Este mÃ©todo es usado en medicina, arqueologÃ­a, biologÃ­a, geofÃ­sica, oceanografÃ­a, ciencia de los materiales y otras ciencias. ...","TomografÃ­a es el procesamiento de imÃ¡genes por secciones. Un aparato usado en tomografÃ­a es llamado tomÃ³grafo, mientras que la imagen producida es un tomograma. Este mÃ©todo es usado en medicina, arqueologÃ­a, biologÃ­a, geofÃ­sica, oceanografÃ­a, ciencia de los materiales y otras ciencias. En la mayorÃ­a de los casos se basa en un procedimiento matemÃ¡tico llamado reconstrucciÃ³n tomogrÃ¡fica. Hay muchos tipos diferentes de tomografÃ­a, tal y como se listan posteriormente (nÃ³tese que la palabra griega tomos conlleva el significado de â€˜cortarâ€™ â€˜dividirâ€™ o â€˜seccionarâ€™). Una tomografÃ­a de varias secciones de un cuerpo es conocida como politomografÃ­a."),
("39","7264039","Patria","1","1","2015-07-12","2015-07-12","2015-05-15","img_noticias/descarga_2_1.jpg","","","","En toda organizaciï¿½n, instituciï¿½n o empresa, la administraciï¿½n en base a los 5 principios fundamentales de la misma: (Planificaciï¿½n, Organizaciï¿½n, Ejecuciï¿½n, Supervisiï¿½n y Evaluaciï¿½n), es clave para el buen desarrollo de toda actividad. Esto nos lleva a visualizar la necesidad de una buena administraciï¿½n en la instalaciï¿½n de cada Escuela Bï¿½blica.","En toda organizaciï¿½n, instituciï¿½n o empresa, la administraciï¿½n en base a los 5 principios fundamentales de la misma: (Planificaciï¿½n, Organizaciï¿½n, Ejecuciï¿½n, Supervisiï¿½n y Evaluaciï¿½n), es clave para el buen desarrollo de toda actividad. Esto nos lleva a visualizar la necesidad de una buena administraciï¿½n en la instalaciï¿½n de cada Escuela Bï¿½blica."),
("40","7264039","Patria 2","2","1","2015-07-28","2015-07-28","2015-07-27","img_noticias/BOMBA.jpg","img_noticias/CAMA_MEDICA.jpg","","","Cree un prospecto llamativo\n\nPara sustituir el texto de un marcador de posición (como este), haga clic en el mismo y empiece a escribir. \n\n?	Se han utilizado estilos para dar formato al texto de esta plantilla. Consulte la galería Estilos de la pestaña Inicio para aplicar un estilo con un simple clic.\n\n?	Para sustituir una foto, haga clic con el botón derecho en la misma y seleccione Cambiar imagen.\n\n?	Para probar otros aspectos para este prospecto, consulte las galerías Temas, Fuentes y Colores de la pestaña Inicio.\n\n?	Si le gusta el aspecto profesional de esta plantilla, use las plantillas de folleto y postal correspondientes para lograr un aspecto profesional y de marca en tan solo unos minutos.\n\n","Cree un prospecto llamativo\n\nPara sustituir el texto de un marcador de posición (como este), haga clic en el mismo y empiece a escribir. \n\n?	Se han utilizado estilos para dar formato al texto de esta plantilla. Consulte la galería Estilos de la pestaña Inicio para aplicar un estilo con un simple clic.\n\n?	Para sustituir una foto, haga clic con el botón derecho en la misma y seleccione Cambiar imagen.\n\n?	Para probar otros aspectos para este prospecto, consulte las galerías Temas, Fuentes y Colores de la pestaña Inicio.\n\n?	Si le gusta el aspecto profesional de esta plantilla, use las plantillas de folleto y postal correspondientes para lograr un aspecto profesional y de marca en tan solo unos minutos.\n\n");




CREATE TABLE `productos` (
  `cod_prod` varchar(13) NOT NULL COMMENT 'Este campo aplica para producto y equipo, en equipo es como el bien nacional',
  `nomb_prod` varchar(100) NOT NULL COMMENT 'nombre de producto',
  `cantidad` int(5) DEFAULT NULL COMMENT 'cantidad de equipos y/o productos',
  `status_p` int(1) NOT NULL COMMENT 'estado del producto o equipo.',
  `fecha_caducidad` date NOT NULL,
  `fecha_registro` date NOT NULL COMMENT 'fecha de registro en el sistema',
  `fecha_actualiza` date NOT NULL COMMENT 'fecha en la que se hizo alguna modificacion al registro',
  `observacion` varchar(140) DEFAULT NULL COMMENT 'acotaciones de la operacion de registro',
  PRIMARY KEY (`cod_prod`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


INSERT INTO productos VALUES
("preeee","AGUA AL 200%","569479","1","2015-07-13","2015-05-01","2015-10-28","POR DOCENAS "),
("C2345","AGUJAS  CU","1687613","1","2015-07-31","2015-04-27","2015-10-28","listas"),
("P420","Agujas de suturas","4611268","1","2015-05-31","2015-04-22","2015-10-28","En cajas de 500 unidades cada una"),
("P887","Bisturis","8778789","1","2018-10-31","2015-10-28","2015-10-28","Por docenas");




CREATE TABLE `recuperaciones` (
  `id_recuperacion` int(5) NOT NULL AUTO_INCREMENT COMMENT 'numero de recuperacion',
  `fecha_solicitud` datetime NOT NULL COMMENT 'fecha en que se hace la solcitud',
  `fecha_actualiza` datetime NOT NULL COMMENT 'fecha en que se hace el proceso y se recupera la informacion',
  `cedula` int(12) NOT NULL COMMENT 'cedula del usuario que solicita la recuperacion',
  `codigo` varchar(10) NOT NULL COMMENT 'codigo aleatorio de validacion',
  `status_recupera` int(1) NOT NULL COMMENT 'status de la validacion de clave',
  PRIMARY KEY (`id_recuperacion`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;






CREATE TABLE `reparaciones` (
  `id_reparacion` int(7) NOT NULL AUTO_INCREMENT COMMENT 'numero de reparacion',
  `cod_e` varchar(50) NOT NULL COMMENT 'serial de equipo numero de bien nacional',
  `cedula` int(12) NOT NULL COMMENT 'cedula del administrador que registra los datos',
  `status_r` int(1) NOT NULL COMMENT '1: programada, 2: realizada, 3: eliminada',
  `fecha_prog` date NOT NULL COMMENT 'fecha de programacion',
  `fecha_actualiza` date NOT NULL COMMENT 'fecha de realizacion del mantenimiento o reparacion',
  `tipo_reparacion` int(1) DEFAULT NULL COMMENT '1: Preventivo, 2: Correctivo',
  `descripcion` varchar(240) DEFAULT NULL COMMENT 'descripcion de la reparacion',
  PRIMARY KEY (`id_reparacion`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;






CREATE TABLE `salidas` (
  `id_salida` int(6) NOT NULL AUTO_INCREMENT COMMENT 'numero de referencia',
  `fecha_salida` date NOT NULL COMMENT 'fecha de registro del pedido del usuario',
  `cedula` int(12) NOT NULL COMMENT 'cedula del usuario que pide el material',
  `cedula_entrega` int(13) DEFAULT NULL COMMENT 'cedula de quien hace la entrega del material',
  `fecha_entrega` date DEFAULT NULL COMMENT 'fecha de entrega del material',
  `status` int(1) NOT NULL COMMENT '1= pendiente, 2=entregado, 3=cancelado',
  PRIMARY KEY (`id_salida`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;






CREATE TABLE `salidas_e` (
  `id_salida_e` int(6) NOT NULL AUTO_INCREMENT COMMENT 'numero de referencia',
  `fecha_salida` date NOT NULL COMMENT 'fecha de registro del pedido del usuario',
  `cedula` int(12) NOT NULL COMMENT 'cedula del usuario que pide el material',
  `depart_prestamo` varchar(80) NOT NULL COMMENT 'departamento al que presta servicios la persona que retira el equipo',
  `retira` varchar(50) NOT NULL COMMENT 'nombre y apellido de quien retira el equipo',
  `ced_retira` int(12) NOT NULL COMMENT 'cedula de quien retira el equipo',
  `cedula_entrega` int(13) DEFAULT NULL COMMENT 'cedula de quien hace la entrega del material',
  `fecha_entrega` date DEFAULT NULL COMMENT 'fecha de entrega del material',
  `status` int(1) NOT NULL COMMENT '1= pendiente, 2=entregado, 3=cancelado',
  `observaciones` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id_salida_e`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;






CREATE TABLE `usuarios` (
  `cedula` int(12) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellido` varchar(35) NOT NULL,
  `telefono` bigint(16) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `direccion` varchar(240) DEFAULT NULL,
  `fecha_registro` date NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(15) NOT NULL,
  `privilegio` int(1) NOT NULL COMMENT 'administrador =1, usuario= 2',
  `status` varchar(1) NOT NULL,
  `cargo` varchar(100) NOT NULL COMMENT 'cargo del usuario dentro del hospital',
  `departamento` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`cedula`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


INSERT INTO usuarios VALUES
("72640321","Joseeee","Ramon","4241624517","jrsp@yahoo.com","Maracay 11","2015-05-03","jose","123","2","1","licenciado","UCI"),
("7264039","Carlos","Silva","1624516","silvagca@yahoo.com","Petare","2015-04-27","carlos","123","1","1","tsu",""),
("141414","Elias","Perdomo","41444411115","kjhkjh@kjhkjh.com","cagua","2015-05-11","dani","123","1","1","tsu",""),
("2213345","SANCHO","SANCHEZ","1214567899","sancho@yahoo.com","barinas","2015-06-15","sancho","123","1","2","licenciado",""),
("45782000","panrcacio","sinforoso","4125463695","pancarsio@hotmail.com","La atlantida","2015-07-28","pancra","123","2","1","tsu",""),
("12345678","Juan Jose","Perez Cruz","4242456952","mmmm@gmail.com","Av. Caracas Juan Perez","2015-09-23","m1m2m3","m1m2m3","1","1","tsu","TRAUMATOLOGIA");




CREATE TABLE `usuarios_autoriza` (
  `cedula` int(12) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellido` varchar(35) NOT NULL,
  `telefono` bigint(16) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `direccion` varchar(240) DEFAULT NULL,
  `fecha_registro` date NOT NULL,
  `cargo` varchar(100) NOT NULL COMMENT 'cargo del usuario dentro del hospital',
  `departamento` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`cedula`),
  UNIQUE KEY `cedula` (`cedula`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


INSERT INTO usuarios_autoriza VALUES
("22331144","JUAN","PEREZ","4242456952","mmmz@gmail.com","Prueba","2015-09-26","aux","UCI"),
("2213233","JOAN","STIX","4242456952","ejjj@dmc.com","Prueba","2015-09-26","tsu","UCI");




CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER VIEW `v_salida_lista` AS select `a`.`id_salida` AS `id_salida`,`a`.`status` AS `status`,`a`.`fecha_entrega` AS `fecha_entrega`,`a`.`cedula` AS `cedula`,`b`.`nombre` AS `nombre`,`b`.`apellido` AS `apellido` from (`salidas` `a` join `usuarios` `b` on((`a`.`cedula` = `b`.`cedula`)));






CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER VIEW `v_salida_prod` AS select `a`.`id_salida` AS `id_salida`,`b`.`fecha_entrega` AS `fecha_entrega`,`b`.`status` AS `status`,`a`.`cod_prod` AS `cod_prod`,`c`.`nomb_prod` AS `nomb_prod`,`a`.`cantidad_prod` AS `cantidad_prod` from ((`detalle_salidas` `a` left join `salidas` `b` on((`a`.`id_salida` = `b`.`id_salida`))) left join `productos` `c` on((`a`.`cod_prod` = `c`.`cod_prod`))) order by `a`.`cod_prod`;





