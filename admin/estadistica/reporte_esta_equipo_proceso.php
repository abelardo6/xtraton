<?php 
    include "ConexBd.php";
    $conn=new ConexBd();
    $db=$conn->db;    
    $fecha_inicio=$_POST['fecha_inicio'];
    $fecha_fin=$_POST['fecha_fin'];

    $idconn=$conn->conectar();
    
    $uci=0;
    $trauma=0;
    $emergencia=0;
    $pediatria=0;
    $d_consulta=0;
    $d_maternidad=0;
    $d_cirugia=0;
    $d_medicina_i=0;
    $d_quirofano=0;
    $d_toxicologia=0;
    $d_central=0;
    $d_hospitalizacion=0;
    $d_cirugia_plas=0;
    
    
    $panel=false;

    $conn->seleccionarBd($db, $idconn);
    
    if ( isset($_POST["depto"])) {
        $panel=true;
        $seldpto=$_POST['depto'];        
	$sql31="SELECT * FROM salidas_e, usuarios WHERE depart_prestamo = '$seldpto' AND salidas_e.cedula=usuarios.cedula AND salidas_e.status='2' AND salidas_e.fecha_salida BETWEEN '$fecha_inicio' AND '$fecha_fin'";
	$sql32="SELECT * FROM salidas_e, detalle_sal_equipos, equipos WHERE depart_prestamo = '$seldpto' AND salidas_e.id_salida_e=detalle_sal_equipos.id_salida_e AND equipos.cod_e=detalle_sal_equipos.cod_e AND salidas_e.status='2' AND salidas_e.fecha_salida BETWEEN '$fecha_inicio' AND '$fecha_fin' ORDER BY salidas_e.id_salida_e ASC";
	$ins31=$conn->transacciones($sql31,$idconn);
	$ins32=$conn->transacciones($sql32,$idconn);
        $valida=1;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    $sql1="SELECT * FROM salidas_e, usuarios WHERE fecha_entrega BETWEEN '$fecha_inicio' AND '$fecha_fin' AND salidas_e.cedula=usuarios.cedula AND salidas_e.status='2'";
    $sql2="SELECT * FROM salidas_e, detalle_sal_equipos, equipos WHERE fecha_entrega BETWEEN '$fecha_inicio' AND '$fecha_fin' AND salidas_e.id_salida_e=detalle_sal_equipos.id_salida_e AND equipos.cod_e=detalle_sal_equipos.cod_e AND salidas_e.status='2' ORDER BY salidas_e.id_salida_e ASC";
    $ins=$conn->transacciones($sql1,$idconn);
    $ins1=$conn->transacciones($sql2,$idconn);
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    $sql="SELECT * FROM salidas_e  Where fecha_entrega BETWEEN '$fecha_inicio' AND '$fecha_fin' AND status='2'";
    $result1=$conn->transacciones($sql,$idconn);
    while($row=mysql_fetch_array($result1)){    
        if(strtoupper($row['depart_prestamo'])=="UCI"){$uci=$uci+1;}
        if(strtoupper($row['depart_prestamo'])=="TRAUMATOLOGIA"){$trauma=$trauma+1;}
        if(strtoupper($row['depart_prestamo'])=="EMERGENCIA"){$emergencia=$emergencia+1;}
        if(strtoupper($row['depart_prestamo'])=="PEDIATRIA"){$pediatria=$pediatria+1;}
        if(strtoupper($row['depart_prestamo'])=="CONSULTA"){$d_consulta=$d_consulta+1;}
        if(strtoupper($row['depart_prestamo'])=="MATERNIDAD"){$d_maternidad=$d_maternidad+1;}
        if(strtoupper($row['depart_prestamo'])=="CIRUGIA"){$d_cirugia=$d_cirugia+1;}
        if(strtoupper($row['depart_prestamo'])=="MEDICINA INTERNA"){$d_medicina_i=$d_medicina_i+1;}
        if(strtoupper($row['depart_prestamo'])=="QUIROFANO"){$d_quirofano=$d_quirofano+1;}
        if(strtoupper($row['depart_prestamo'])=="TOXICOLOGIA"){$d_toxicologia=$d_toxicologia+1;}
        if(strtoupper($row['depart_prestamo'])=="CENTRAL DE SUMINISTRO"){$d_central=$d_central+1;}
        if(strtoupper($row['depart_prestamo'])=="HOSPITALIZACION"){$d_hospitalizacion=$d_hospitalizacion+1;}
        if(strtoupper($row['depart_prestamo'])=="CIRUGIA PLASTICA"){$d_cirugia_plas=$d_cirugia_plas+1;}
    }
	
    if(!$ins){echo"buen>".mysql_error();}
?>
<!DOCTYPE HTML>
<html>
	<head>
	<!--css y js de tabla-->
		<link rel="stylesheet" type="text/css" href="css_tabla/bootstrap.min.css" />
		<!--<link rel="stylesheet" type="text/css" href="css_tabla/style.css" />-->
		<script type="text/JavaScript" src="js_tabla/jquery.min.js"></script>
		<script type="text/JavaScript" src="js_tabla/bootstrap.min.js"></script>
	<!--fin css y js tabla-->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Estadistica Salida Equipos</title>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
function limpiarError()
{
    document.getElementById("nombreError2").innerHTML="";

    return true;
}

$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Grafico: Comportamiento Prestamos equipos medicos'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.3f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Departamentos',
            data: [
                ['UCI',   <?php echo $uci?>],
                ['Traumatologia',   <?php echo $trauma?>],
                ['Emergencia',   <?php echo $emergencia?>],
                ['Pediatria',   <?php echo $pediatria?>],
                ['CONSULTA', <?php echo $d_consulta?>],
                ['MATERNIDAD', <?php echo $d_maternidad?>],
                ['CIRUGIA', <?php echo $d_cirugia?>],
                ['MEDICINA INTERNA', <?php echo $d_medicina_i?>],
                ['QUIROFANO', <?php echo $d_quirofano?>],
                ['TOXICOLOGIA', <?php echo $d_toxicologia?>],
                ['CENTRAL DE SUMINISTRO', <?php echo $d_central?>],
                ['HOSPITALIZACION', <?php echo $d_hospitalizacion?>],
                ['CIRUGIA PLASTICA', <?php echo $d_cirugia_plas?>],                                
            ]
        }]
    });
});
</script>
</head>
<body>	
<div  style="width:800px; margin:0px auto;margin-top:30px;border:1px solid #cecece;">	
    <ul id="brochure"class="nav nav-tabs">
        <?php 
        if ($panel) {
            echo '<li><a href="#grafico" data-toggle="tab">Gr&aacute;fico Estad&iacute;stico</a></li>';
            echo '<li><a href="#listado" data-toggle="tab">Listado Datos</a></li>';
            echo '<li><a href="#salidas" data-toggle="tab">Por Departamento</a></li>';
        } else {
            echo '<li><a href="#grafico" data-toggle="tab">Gr&aacute;fico Estad&iacute;stico</a></li>';
            echo '<li><a href="#listado" data-toggle="tab">Listado Datos</a></li>';
            echo '<li><a href="#salidas" data-toggle="tab">Por Departamento</a></li>';
        }
        ?>                
    </ul>
    <!-- BEGIN TABS 1 -->
    <div id="brochure-content" class="tab-content">
        <?php 
        if ($panel) {
            echo '<div class="tab-pane fade" id="grafico">';
        } else {
            echo '<div class="tab-pane fade active in" id="grafico">';
        }
        ?>                
            <script src="js/highcharts.js"></script>
            <script src="js/modules/exporting.js"></script>
            <div id="container" style="min-width: 500px; height: 400px; max-width: 600px; margin: 0 auto;border:px solid #000000;margin-bottom:20px;"></div>
        </div>	
        <!-- BEGIN TABS 2 -->
        <div class="tab-pane fade" id="listado">				
            <h2 align="center">Listado de Equipos Prestados</span></h2>
            <br />
            <table align="center" border="1" width="600">
            <td colspan="7"style="background:#ede930;"><center><strong>Responsable</strong></center></tr>
            <th>Nro.</th>
            <th>Persona que Retira</th>
            <th>Cedula</th>
            <th>Departamento</th>
            <th>Fecha Pedido</th>        
            <?php
            //$contador=0;
            $row=mysql_fetch_array($ins);
            do{
            //$contador=$contador+1;
            ?>       
                <tr style="background:#ccccc5; text-align:center;">
                <td><div style="width:20px;"><?php echo $row['id_salida_e'];?></div></td>
                <td><div style="width:180px;"><?php echo $row['retira'];?></div></td>
                <td><div style="width:100px;"><?php echo $row['ced_retira'];?></div></td>
                <td><div style="width:130px;"><?php echo $row['depart_prestamo'];?></div></td>
                <td><div style="width:100px;"><?php echo $row['fecha_salida'];?></div></td>
                </tr>
            <?php
            }while($row=mysql_fetch_array($ins));
            ?>
            <table align="center" border="1" width="600">
                <tr>
                <td colspan="7"style="background:#ede930;"><center><strong>Detalle de los Pedidos</strong></center></tr>
                </tr>
                <?php
                while($row1=mysql_fetch_array($ins1)){
                ?>
                    <tr style="background:#ccccc5;">
                    <td style="font-weight:bolder;">Num. Pedido:</td>
                    <td><?php echo $row1['id_salida_e'];?></td>
                    <td style="font-weight:bolder;">Equipo:</td>
                    <td><?php echo $row1['nomb_e'];?></td>
                    </tr>
                <?php } ?>
            </table>    
            <table border="0" align="center">
                <tr>
                <td align="center" colspan="4"><a id="img_print" href="javascript:void(0);" onClick="document.getElementById('img_print').style.display='none';window.print();document.getElementById('img_print').style.display='inline'"><img src="impresora.jpg" border=0 width="100" height="100"></a></td>
                </tr>
                <!--FIN TABLA DE COMPROBANTE-->
            </table>
	</div>
        <?php 
        if ($panel) {
            echo '<div class="tab-pane fade active in" id="salidas">';
        } else {
            echo '<div class="tab-pane fade" id="salidas">';
        }
        ?>        
            <!-- BEGIN TABS 3 -->
            <table width="99%" cellpadding="0" cellspacing="0">
                <form name="form1" id="form1" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" onsubmit="" onreset="return limpiarError();">
                    <tr>
                        <td colspan="4"align='center'><strong>Departamento:</strong>
                            <select name="depto">
                                <option value="CONSULTA">Consulta</option>
                                <option value="UCI">UCI</option>
                                <option value="TRAUMATOLOGIA">Traumatologia</option>
                                <option value="EMERGENCIA">Emergencia</option>
                                <option value="PEDIATRIA">Pediatria</option>
                                <option value="MATERNIDAD">Maternidad</option>
                                <option value="CIRUGIA">Cirugia</option>
                                <option value="MEDICINA INTERNA">Medicina Interna</option>
                                <option value="QUIROFANO">Quirofano</option>
                                <option value="TOXICOLOGIA">Toxicologia</option>
                                <option value="CENTRAL DE SUMINISTRO">Central de Suministro</option>
                                <option value="HOSPITALIZACION">Hospitalizacion</option>
                                <option value="CIRUGIA PLASTICA">Cirugia Plastica</option>
                            </select>
                            <samp><input type="submit" id="buscarnro" value="Buscar"/></samp>                        
                        </td>
                    </tr>
                    
                    <h2 align="center">Detalles por prestamos de Equipos Medicos</span></h2>
                    <br />
                    <table align="center" border="1" width="600">
            <td colspan="7"style="background:#ede930;"><center><strong>Responsable</strong></center></tr>
                    <th>Nro.</th>
                    <th>Persona que Retira</th>
                    <th>Cedula</th>
                    <th>Departamento</th>
                    <th>Fecha Pedido</th>        
                    <?php
                    //$contador=0;
                    @$row=mysql_fetch_array(@$ins31);
                    do{
                    //$contador=$contador+1;
                    ?>       
                        <tr style="background:#ccccc5; text-align:center;">
                        <td><div style="width:20px;"><?php echo $row['id_salida_e'];?></div></td>
                        <td><div style="width:180px;"><?php echo $row['retira'];?></div></td>
                        <td><div style="width:100px;"><?php echo $row['ced_retira'];?></div></td>
                        <td><div style="width:130px;"><?php echo $row['depart_prestamo'];?></div></td>
                        <td><div style="width:100px;"><?php echo $row['fecha_salida'];?></div></td>
                        </tr>
                    <?php
                    }while(@$row=mysql_fetch_array(@$ins31));
                    ?>
                    <table align="center" border="1" width="600">
                        <tr>
            <td colspan="7"style="background:#ede930;"><center><strong>Detalle de los Pedidos realizados </strong></center></tr>
                        </tr>
                        <?php
                        while(@$row1=mysql_fetch_array(@$ins32)){
                        ?>
                            <tr style="background:#ccccc5;">
                            <td style="font-weight:bolder;">Num. Pedido:</td>
                            <td><?php echo $row1['id_salida_e'];?></td>
                            <td style="font-weight:bolder;">Equipo:</td>
                            <td><?php echo $row1['nomb_e'];?></td>
                            <td style="font-weight:bolder;">Observacion:</td>
                            <td><?php echo $row1['observaciones'];?></td>
                            </tr>
                        <?php                         
                        } 
                        ?>
                        <tr>
                            <td><input type="hidden" name="fecha_inicio" value="<?php echo $fecha_inicio;?>"></td>
                            <td><input type="hidden" name="fecha_fin" value="<?php echo $fecha_fin;?>"></td>
                        </tr>
                    </table>    
                        
                    <table border="0" align="center">
                        <tr>
                        <br>
                        <br>
                        <td align="center" colspan="4"><a id="img_print2" href="javascript:void(0);" onClick="document.getElementById('img_print2').style.display='none';window.print();document.getElementById('img_print2').style.display='inline'"><img src="impresora.jpg" border=0 width="100" height="100"></a></td>
                        </tr>
                    </table>
                        
                </form>
                <br></br>
            </table>            
            
        </div>	        
    </div>
</div>
</body>
</html>
