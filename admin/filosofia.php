<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Nuestra Filosofia</title>
<link href="style.css" rel="stylesheet" type="text/css" />

</head>
<body>
<div id="contenedor">
    <div id="cabecera">
       <div id="top">
	</div>
<div id="top1">
</div>

<?php include("includes/menu_superior_inicio.php");?>
</div>
    <div id="cuerpo">
       <div id="lateral">
			<!--<h3 align="center">ENLACES</h3>-->
          <ul>
                <li><a href="http://www.fundaribas.gob.ve/paginaweb/#" target="_blank"><img src="images/josefelix.jpg" width="170px" height="120px" /></a></li>
             <li><a href="http://sacs.mpps.gob.ve/siacs/" target="_blank"><img src="images/contraloria.jpg" width="170px" height="120px" /></a></li>
             <li><a href="http://www.derechos.org.ve/tag/programa-nacional-de-atencion-en-salud-para-las-personas-con-discapacidad/" target="_blank"><img src="images/pasdis.jpg" width="170px" height="120px" /></a></li>

           
          </ul>
       </div>
       <div id="otrolado">
          <!--<h3 align="center">ENLACES</h3>-->
          <ul>
                 <li><a href="http://www.ivss.gov.ve/" target="_blank"><img src="images/ivss.jpg" width="150px" height="120px" /></a></li>
             <li><a href="http://www.mpps.gob.ve/" target="_blank"><img src="images/mpps.jpg" width="150px" height="120px" /></a></li>
			 <li><a href="http://www.minpal.gob.ve/" target="_blank"><img src="images/ministerio.jpg" width="150px" height="120px" /></a></li>

          </ul>
       </div>
       <div id="principal">
          <article><center><h3>FILOSOFIA DE LA ENFERMERIA EN U.C.I</h3></center><center><h5>&quot; UN SER MUY ESPECIAL PARA UN AREA MUY ESPECIAL &quot;</h5></center>Dentro del contexto de la atenci&oacute;n progresiva del enfermo, consideramos a la Unidad de Cuidados Intencivos (U.C.I.) como la fase de mayor transcendencia para la &oacute;ptima recuperaci&oacute;n de este. 
		  El concepto de atenci&oacute;n al enfermo cr&iacute;tico, emerge de la enfermera, ocupando la c&uacute;spide de la pir&aacute;mide e integrando todo el sistema de atenci&oacute;n.<br/><br/>
		  La enfermera U.C.I. debe tener como norma intr&iacute;nseca e invariable la disposicion de dedicarse en forma directa, continua  y sin interferencias a la atenci&oacute;n del enfermo cr&iacute;tico.</article>
		  <article>Todo esto se logra mediante un equipo de enfermer&iacute;a altamente capacitado y que este comprometido con el concepto de cuidados intensivos, que  demuestre su idoneidad t&eacute;cnica, que tenga capacidad  para el trabajo coordinado, que sea capaz de trabajar bajo stress, de mantener los principios de las relaciones humanas, que sepa dirigir, ense&ntilde;ar y que pueda elevar en forma genuina el concepto de la enfermera en la U.C.I.</article>
		  <article><img style="float: left;" src="images/florence.png">Enfermera inglesa, pionera de la enfermer&iacutea profesional moderna (Florencia, 1820 - Londres, 1910). Procedente de una familia rica, rechaz&oacute la c&oacutemoda vida social a la que estaba destinada, para trabajar como enfermera desde 1844. Motivada por sus deseos de independencia y por sus convicciones religiosas, se enfrent&oacute a su familia y a los convencionalismos sociales de la &eacutepoca para buscar una cualificaci&oacuten profesional que le permitiera ser &uacutetil a la Humanidad. .</article><br /><br />
		</div>
    <?php include("includes/footer_index.php");?>
</div>

</body>
</html>