<?php
// una variable la primera vez que se carga la pagina web, esta no esta definida aun,
// posteriormente despues de cargar la pagina o recargarla independientemente de que
// tenga un dato o no, ya queda definida, por eso la funcion isset() es valida para la 
// primera vez que se carga la pagina web

// por otra parte la funcion strlen() solo funciona si la variable ya fue definida, en 
// caso contrario da un error de advertencia, indicando que la variable no esta definida

//echo isset($_POST["usuario_l"]) ? "El campo usuario (SI) esta definido" : "El campo usuario (NO) esta definido";

if (isset($_POST["usuario_l"])) {
// inicializo datos formulario login
$usuario_l = $_POST["usuario_l"];
$clave_l = $_POST["clave_l"];
//$_SERVER['PHP_SELF'] = "";

// botones del formulario login
$busca_l = isset($_POST["busca_l"]) ? $_POST["busca_l"]:"";
$limpia_l = isset($_POST["limpia_l"]) ? $_POST["limpia_l"]:"";
}else{
// la primera vez que carga la pagina web se incializan las variables
//$_SERVER['PHP_SELF'] = "";
$usuario_l = "";
$clave_l = "";
$privilegio_l = "";
$busca_l = "";
$limpia_l = "";

}

// buscar un usuario
if ($usuario_l != NULL && $busca_l == "Entrar") {
	include "ConexBd.php";
	$conn=new ConexBd();
	$db=$conn->db;	
        //abrimos conexion
        $idconn=$conn->conectar();
	$sbd=mysqli_select_db($idconn,$db);
	//$query = "SELECT usuario_id, clave, nombre, apellido, cedula, fecha, privilegio FROM usuario where usuario_id = '$usuario_l' and clave = '$clave_l'";
	$query ="SELECT * FROM usuarios WHERE usuario='$usuario_l' and clave = '$clave_l' and status=1";
	$result = mysqli_query($idconn,$query) or die(mysqli_error());

	if (mysqli_num_rows($result)>0) {
		$resp_l = "Usuario Localizado";
		$rowEmp = mysqli_fetch_array($result);
		//$usuario_l = $r["usuario"];
		//$clave_l = $r["clave"];
		$privilegio_l = $r["privilegio"];
		if(!isset($_SESSION)){session_start();}
				//creamos variables de sesion con los datos del usuario para luego poder usarlas mientras 
				//la sesion este abierta
				$_SESSION["administrador"]=	"si";
				$_SESSION["nombre"]=$rowEmp['nombre_usuario'];
				$_SESSION["apellido"]=$rowEmp['apellido_usuario'];
				$_SESSION["usuario"]=$rowEmp['usuario'];
				$_SESSION["privilegio"]=$rowEmp['privilegio'];
				$_SESSION["cedula"]=$rowEmp['rut_usuario'];
				header('Location:index_admin.php');
	}else{
		$resp_l = "Usuario invalido o clave invalida";
	}
	mysqli_free_result($result);
//se cierra los casos para usuarios encontrados y no encontrados
	}else {
	$resp_l = "Favor de colocar el usuario y la clave";
}

// limpiar el formulario
if ($limpia_l == "Limpiar") {
	$usuario_l = "";
	$clave_l = "";
	$privilegio_l = "";
} 

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Administrador</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<SCRIPT language="JavaScript" type="text/javascript">
   
   <!--La funci? chequeoFinal permite verificar que los campos obligatorios hayan sido llenados-->
     function chequeoFinal(){
        var control=false;
		if(chequear(document.f.usuario, "Usuario"))
		if(chequear(document.f.clave, "Clave"))
		control=true;
        if(control){
		
             alert("Verificacion de campos completada con exito!!!");
        }else{
            return false;
        }
		}

		function chequear(k, nomb) {
    <!--Usado para verificar los campos vac?s-->
        if(k.value.length==0){
            alert("Lo siento "+ nomb +" no puede estar vacio");
			k.focus();
            return false;
        } else {
            return true;
        }
    }
	</SCRIPT>
</head>
<body>
<div id="contenedor">
    <div id="cabecera">
       <div id="top">
	</div>
<div id="top1">
</div>

<?php include("includes/menu_superior_inicio.php");?>
</div>
    <div id="cuerpo">
       <div id="principal">
           <center>
			<h2 class="titlat">Iniciar Sesion</h2>
<div id="flogin" class="cuerpolateral">
<TABLE  CELLSPACING=1 CELLPADDING=1>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST" id="loginForm">
<TR><TD width="20%">Usuario:</TD> <TD width="80%"><input type="text" id="usuario" name="usuario_l" size="40"  value="<?php $usuario_l; ?>"></TD></TR>
<TR><TD width="20%">Clave:</TD> <TD width="80%"><input type="password" id="clave" name="clave_l" size="40"></TD></TR>
<TR><TD width="20%">&nbsp;</TD><TD width="80%"><input style="border:1px solid #ffffff;"type="text" name="resp_l" size="40" maxlength="40" value="<?php echo $resp_l;?>"></TD></TR>

</TABLE>
<br />
<TABLE  CELLSPACING=1 CELLPADDING=1>
<TR>
<TD><input type="submit" name="busca_l" value="Entrar"></TD>
<td>&nbsp;</td>
<TD><input type="submit" name="limpia_l" value="Limpiar"></TD>
</TR>
</form>
</TABLE>
</div>
			<a href="recupera_datos_form.php" target="_self"><h5>&iquest;olvid&oacute; su contrase&ntilde;a?</h5></a>
			</center>
       </div>
    </div>
    <?php include("includes/footer_index.php");?>
</div>
</body>
</html>