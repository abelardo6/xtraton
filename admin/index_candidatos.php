<?php
	include "ConexBd.php";
	$conn=new ConexBd();
	$db=$conn->db;
	
		//abrimos conexion
		$idconn=$conn->conectar();
		//seleccionamos la bd
		$conn->seleccionarBd($idconn,$db);
		//busqueda productos
		$sql1="SELECT * FROM candidatos, usuarios where candidatos.rut_usuario=usuarios.rut_usuario ORDER BY candidatos.fecha_registro_candidato DESC";
		$ins1=$conn->transacciones($idconn,$sql1);
		$row1 = mysqli_fetch_assoc($ins1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--estilos tabla-->

<style type="text/css" title="currentStyle">
			@import "media/css/demo_page.css";
			@import "media/css/demo_table.css";
		</style>
		<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable( {
					"sPaginationType": "full_numbers"
				} );
			} );
		</script>
  
<!--FIN ESTILOS TABLA-->
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">  

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Operaciones con Candidatos</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>
<SCRIPT language="JavaScript" type="text/javascript">
   
   <!--La funci? chequeoFinal permite verificar que los campos obligatorios hayan sido llenados-->
     function chequeoElimina(){
		var r = confirm("Desea eliminar el Registro?");
		if (r == true) {
			
			return true;
		} else {
			return false;
		}	 
		}
		function chequeoElimina1(){
		var r = confirm("Desea eliminar el Registro?");
		if (r == true) {
			
			return true;
		} else {
			return false;
		}	 
		}
	</SCRIPT>
   
	
</head>
<body>
	<!--top part start -->
	<div id="wrap">
	<div id="top">
	</div>
	<div id="top1">
	</div>
	<!--top part end -->
    <!--body start -->
<div id="body">
	<br class="spacer" />
  
  
   
   <!--mid panel start -->
  <div id="mid_admin_rep">
  <div class="fondo_azul">
	<?php include("includes/menu_superior.php");?>
		</div>
   <br /><br /><br /><br />
  <!--inicio script-->
  <h2 align="center">Candidatos</h2>
	<div id="demo"style="margin-bottom:70px;margin-top:40px;">
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>RUT.</th>
			<th>Nombre y Apellido</th>
			<th>Email</th>
			<th>Ciudad Residencia</th>
			<th>Fecha Registro</th>
			<th>Agente atencion</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php do{?>
		<tr class="gradeC">	
			<td><?php echo $row1['rut_candidato'];?></td>
			<td><?php echo $row1['nombre_candidato'];?> <?php echo $row1['apellido_candidato'];?></td>
			<td><?php echo $row1['email_candidato'];?></td>
			<td class="center"><?php echo $row1['ciudad_residencia'];?></td>
			<td class="center"><?php echo $row1['fecha_registro_candidato'];?></a></td>
			<td class="center"><?php echo $row1['nombre_usuario'];?> <?php echo $row1['apellido_usuario'];?></a></td>
			<td><a href="actualiza_producto_proceso.php?cod=<?php echo $row1['rut_candidato']; ?>"><img src="images/lapiz.png"width="30"height="30" title="Editar Candidato"/></td>
			<td><a href="elimina_candidato_proceso.php?cod=<?php echo $row1['rut_candidato']; ?>"onClick="return chequeoElimina()"><img src="images/equis.png"width="40"height="40" title="Eliminar Candidato"/></a></td>
			<td><a href="ver_ficha_candidato.php?cod=<?php echo $row1['rut_candidato']; ?>"target="_blank"><img src="images/historia_act.png"width="40"height="40" title="Ver Ficha"/></a></td>
		</tr>
	<?php }while($row1 = mysqli_fetch_assoc($ins1)); ?>
	</tbody>
	<tfoot>
		<tr>
			<th>RUT.</th>
			<th>Nombre y Apellido</th>
			<th>Email</th>
			<th>Ciudad Residencia</th>
			<th>Fecha Registro</th>
			<th>Agente atencion</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</tfoot>
</table>
<table align="center" border="0"width="500">
		<tr>
			<td align="center"><a href="registro_producto_form.php"><img src="images/editar.jpg"width="80"height="80" title="Registro Repuesto"/></a></td>
		</tr>
	</table><br>
			</div>
  <!--fin script tabla-->
  </div>
   
	<br class="spacer" />
	<br class="spacer" />
	<?php include("includes/footer_index.php"); ?>
	
</div>

	<!--body end -->
	
   </div> 
</body>
</html>