<?php
	include "ConexBd.php";
	$conn=new ConexBd();
	$db=$conn->db;
	
		//abrimos conexion
		$idconn=$conn->conectar();
		//seleccionamos la bd
		$conn->seleccionarBd($db,$idconn);
		//busqueda productos
		$sql1="SELECT * FROM productos";
		$ins1=$conn->transacciones($sql1,$idconn);
		$row1 = mysql_fetch_assoc($ins1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--estilos tabla-->

<style type="text/css" title="currentStyle">
			@import "media/css/demo_page.css";
			@import "media/css/demo_table.css";
		</style>
		<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable( {
					"sPaginationType": "full_numbers"
				} );
			} );
		</script>
  
<!--FIN ESTILOS TABLA-->
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">  

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Operaciones con Productos/Equipos</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>
<SCRIPT language="JavaScript" type="text/javascript">
   
   <!--La funci? chequeoFinal permite verificar que los campos obligatorios hayan sido llenados-->
     function chequeoElimina(){
		var r = confirm("Desea eliminar el Registro?");
		if (r == true) {
			
			return true;
		} else {
			return false;
		}	 
		}
		function chequeoElimina1(){
		var r = confirm("Desea eliminar el Registro?");
		if (r == true) {
			
			return true;
		} else {
			return false;
		}	 
		}
	</SCRIPT>
   
	
</head>
<body>
	<!--top part start -->
	<div id="wrap">
	<div id="top">
	</div>
	<div id="top1">
	</div>
	<!--top part end -->
    <!--body start -->
<div id="body">
	<br class="spacer" />
  
  
   
   <!--mid panel start -->
  <div id="mid_admin_rep">
  <div class="fondo_azul">
	<?php include("includes/menu_superior.php");?>
		</div>
   <br /><br /><br /><br />
  <!--inicio script-->
  <h2 align="center">Busqueda de Productos</h2>
	<div id="demo"style="margin-bottom:70px;margin-top:40px;">
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>Cod.</th>
			<th>Nombre</th>
			<th>Observacion</th>
			<th>Status</th>
			<th>Cant.</th>
			<th>Fecha Registro</th>
			<th>P. Compra</th>
			<th>P. Venta</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php do{?>
		<tr class="gradeC">	
			<td><?php echo $row1['cod_prod'];?></td>
			<td><?php echo $row1['nombre_prod'];?></td>
			<td><?php echo $row1['observacion'];?></td>
			<td class="center"><?php echo $row1['status_p'];?></td>
			<td class="center"><?php echo $row1['cantidad'];?></a></td>
			<td class="center"><?php echo $row1['fecha_registro'];?></a></td>
			<td class="center"><?php echo $row1['precio_compra'];?></a></td>
			<td class="center"><?php echo $row1['precio_venta'];?></a></td>
			<td><a href="actualiza_producto_proceso.php?cod=<?php echo $row1['cod_prod']; ?>"><img src="images/lapiz.png"width="30"height="30" title="Editar Repuesto"/></td>
			<td><a href="elimina_producto_proceso.php?cod=<?php echo $row1['cod_prod']; ?>"onClick="return chequeoElimina()"><img src="images/equis.png"width="40"height="40" title="Eliminar Repuesto"/></a></td>
		</tr>
	<?php }while($row1 = mysql_fetch_assoc($ins1)); ?>
	</tbody>
	<tfoot>
		<tr>
			<th>Cod</th>
			<th>Nombre</th>
			<th>Observacion</th>
			<th>Compatible</th>
			<th>A&ntilde;o</th>
			<th>Existencia</th>
			<th>Status</th>
			<th>Ubicaci&oacute;n</th>
			<th>Cant.</th>
		</tr>
	</tfoot>
</table>
<table align="center" border="0"width="500">
		<tr>
			<td align="center"><a href="registro_producto_form.php"><img src="images/editar.jpg"width="80"height="80" title="Registro Repuesto"/></a></td>
		</tr>
	</table><br>
			</div>
  <!--fin script tabla-->
  </div>
   
	<br class="spacer" />
	<br class="spacer" />
	<?php include("includes/footer_index.php"); ?>
	
</div>

	<!--body end -->
	
   </div> 
</body>
</html>