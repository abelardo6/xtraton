<?php
	include "ConexBd.php";
	$conn=new ConexBd();
	$db=$conn->db;
	
		//abrimos conexion
		$idconn=$conn->conectar();
		//seleccionamos la bd
		$conn->seleccionarBd($db,$idconn);
		//seleccionamos todos los usuarios que tengan status 1... que esten activos
		$sql="SELECT * FROM usuarios_autoriza";
		$ins=$conn->transacciones($sql,$idconn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style text="text/css">
	.tabladiv{
		text-align:center;
		
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style.css" rel="stylesheet" type="text/css" />

<title>Usuarios</title>
<link rel="stylesheet" type="text/css" href="css_tabla/bootstrap.min.css" />
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="menu2.css" rel="stylesheet" type="text/css" />
<script type="text/JavaScript" src="js_tabla/jquery.min.js"></script>
<script type="text/javascript" language="javascript">


	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>
   
<SCRIPT language="JavaScript" type="text/javascript">
   
   <!--La funci? chequeoFinal permite verificar que los campos obligatorios hayan sido llenados-->
     function chequeoFinal(){
		var r = confirm("Desea eliminar el Registro?");
		if (r == true) {
			
			return true;
		} else {
			return false;
		}	 
		}
	</SCRIPT>	
</head>
<body onload="document.fo.cedula.focus();">
	<!--top part start -->
	<div id="wrap">
	<div id="top">
	</div>
	<div id="top1">
	</div>
	<!--top part end -->
    <!--body start -->
<div id="body">
	<br class="spacer" />
  
  
   <?php
		$total=mysql_num_rows($ins);
	?>
   <!--mid panel start -->
  <div id="mid_admin">
<div class="fondo_azul">
	<?php include("includes/menu_superior.php");?>
		</div><br/>
  <h2 align="center">Usuarios Autorizados para Salida de Equipos</span></h2>
	<br />
    
		<!--tabla que muestra el listado de usuarios registrados-->
		<form name="f" action="registro_consulta_proceso.php" method="post" onSubmit="return chequeoFinal()">
		<table align="center" border="0" width="">
			<th>C&eacute;dula</th>
			<th>Nombre y Apellido</th>
			<th>Tel&eacute;fono</th>
			<th>Email</th>
			<th>Cargo</th>
			<th>Departamento</th>
			<?php
				$contador=0;
				//aqui comienza la iteracion mostrando en filas cada uno de los usuarios encontrados
				while($row=mysql_fetch_array($ins)){
					$contador=$contador+1;
			?>    
			<!--cada fila de los usuarios-->
			<tr>
							

				<td><INPUT type="text" readonly="readonly" value="<?php echo $row['cedula'];?>"size="10"></td>
				<td><INPUT type="text" readonly="readonly" value="<?php echo $row['nombre'];?> <?php echo $row['apellido'];?>"size="16"></td>
				<td><INPUT type="text" readonly="readonly" value="<?php echo $row['telefono'];?>"size="10"></td>
				<td><INPUT type="text" readonly="readonly" value="<?php echo $row['email'];?>"size="20"></td>
				<td><INPUT type="text" readonly="readonly" value="<?php echo $row['cargo'];?>"size="10"></td>
				<td><INPUT type="text" readonly="readonly" value="<?php echo $row['departamento'];?>"size="20"></td>
				<!--enlaces para editar y eliminar un registro-->
				<td><a href="actualiza_usuario_autoriza.php?cod=<?php echo $row['cedula']; ?>"><img src="images/lapiz.png"width="40"height="40" title="Editar Usuario"/></a></td>
				<td><a href="elimina_usuario_autoriza.php?cod=<?php echo $row['cedula']; ?>"onClick="return chequeoFinal()"><img src="images/equis.png"width="40"height="40" title="Eliminar Usuario"/></a></td>
			</tr>
			<?php
				}
			?>
		</table>
		
	</form>
    <table align="center" border="0"width="500">
		<tr>
			<td align="center"><a href="registro_usuario_autoriza.php"><img src="images/editar.JPG"width="80"height="80" title="Registrar Nuevo Usuario"/></a></td>
			<!--<td align="center"><a href="recupera_usuario_form.php">Recupera Usuario</a></td>-->
		</tr>
	</table>
  </div>
   <br />
	<br class="spacer" />
	<br class="spacer" />
<?php include("includes/footer_index.php"); ?>	
</div>
</div>

	<!--body end -->
    
</body>
</html>