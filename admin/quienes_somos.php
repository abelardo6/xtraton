<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Quienes Somos</title>
<link href="style.css" rel="stylesheet" type="text/css" />

</head>
<body>
<div id="contenedor">
    <div id="cabecera">
       <div id="top">
	</div>
<div id="top1">
</div>

<?php include("includes/menu_superior_inicio.php");?>
</div>
    <div id="cuerpo">
       <div id="lateral">
			<!--<h3 align="center">ENLACES</h3>-->
          <ul>
            <li><a href="http://www.fundaribas.gob.ve/paginaweb/#" target="_blank"><img src="images/josefelix.jpg" width="170px" height="120px" /></a></li>
             <li><a href="http://sacs.mpps.gob.ve/siacs/" target="_blank"><img src="images/contraloria.jpg" width="170px" height="120px" /></a></li>
             <li><a href="http://www.derechos.org.ve/tag/programa-nacional-de-atencion-en-salud-para-las-personas-con-discapacidad/" target="_blank"><img src="images/pasdis.jpg" width="170px" height="120px" /></a></li>
          </ul>
       </div>
       <div id="otrolado">
          <!--<h3 align="center">ENLACES</h3>-->
          <ul>
                 <li><a href="http://www.ivss.gov.ve/" target="_blank"><img src="images/ivss.jpg" width="150px" height="120px" /></a></li>
             <li><a href="http://www.mpps.gob.ve/" target="_blank"><img src="images/mpps.jpg" width="150px" height="120px" /></a></li>
			 <li><a href="http://www.minpal.gob.ve/" target="_blank"><img src="images/ministerio.jpg" width="150px" height="120px" /></a></li>

          </ul>
       </div>
       <div id="principal">
          <article><h3>Objetivo General</h3><br/> Lograr que nuestra unidad sea un centro de conocimiento, donde la docencia y la investigaci&oacuten sean parte integral del proceso formativo de todo el personal. </article>
		  <article><h3>Misi&oacute;n</h3><br/> Prestar atenci&oacute;n integral especializada  las 24 horas al d&iacute;a con personal altamente capacitado, eficaz y eficiente para atender al usuario cr&iacute;ticamente enfermo, consider&aacute;ndolo como un ser &uacutenico en sus necesidades  Bio-Psico-Social, con el fin de lograr su recuperaci&oacute;n y reintegraci&oacute;n al medio familiar en el menor tiempo posible utilizando los recursos humanos y materiales necesarios en forma racional .</article>
		  <article><h3>Visi&oacute;n</h3><br/> Nuestros esfuerzos se orientan hacia la formaci&oacute;n y capacitaci&oacute;n de personal altamente especializado, que sea oportuno y eficiente en la atenci&oacute;n del paciente cr&iacute;ticamente enfermo en una forma hol&iacute;stica, al mismo tiempo que sus acciones est&eacute;n encaminadas a la continuaci&oacute;n de la presentaci&oacute;n del servicio de igual calidad fuera de la unidad, es decir, en el area de hospitalizaci&oacute;n, siguiendo los cuidados progresivos, con la participaci&oacute;n integral de todo el equipo de salud.</article>
		</div>
    <?php include("includes/footer_index.php");?>
</div>

</body>
</html>