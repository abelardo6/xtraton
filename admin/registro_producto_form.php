<?php if(!isset($_SESSION)){session_start();}
	if($_SESSION['administrador']!="si"){header("Location: index.php");exit;}
	
	///////////procesar actualizacion///////////////
	// botones del formulario actualizacion
		$enviar = isset($_POST["registrar"]) ? $_POST["registrar"]:"";
		
		if($enviar=="Registrar"){
		
			include "ConexBd.php";  
			$conn=new ConexBd();
			$db=$conn->db;
		
		$fecha_caducidad=$_POST['fecha_caducidad'];
		$cod_prod=$_POST['cod_prod'];
		$nombre=$_POST['nombre'];
		$cantidad=$_POST['cantidad'];
		$status=$_POST['status'];
		$observacion=$_POST['observacion'];
		$fecha=strftime( "%Y-%m-%d", time() );
			
			//abrimos conexion con la base de datos
		$idconn=$conn->conectar();
		//seleccionamos la base de datos
		$conn->seleccionarBd($db,$idconn);
		//creamos la consulta
		$sql="INSERT INTO productos VALUES('".$cod_prod."','".$nombre."','".$cantidad."','".$status."','".$fecha_caducidad."','".$fecha."','".$fecha."','".$observacion."')";
		$ins=$conn->ejecuta_consulta($sql,$idconn);
		$resp_l = "Producto Registrado con Exito";
			if(!$ins){
				$resp_l = "Operacion Fallida Intentelo de Nuevo";
			}
		}else{
			$resp_l = "Formulario Registro de Productos";
		}
		
	////////////////////////////////////////////////
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Registro Producto</title>
<!--INICIO INCLUDES DEL CALENDARIO-->

<link rel="stylesheet" type="text/css" media="all" href="calendario/calendar-green.css" title="win2k-cold-1" />
<!-- librer�a principal del calendario -->
  <script type="text/javascript" src="calendario/calendar.js"></script>
<!-- librer�a para cargar el lenguaje deseado -->
  <script type="text/javascript" src="calendario/calendar-es.js"></script>
<!-- librer�a que declara la funci�n Calendar.setup, que ayuda a generar un calendario en unas pocas l�neas de c�digo -->
  <script type="text/javascript" src="calendario/calendar-setup.js"></script>
  
<!--FIN INCLUDES DEL CALENDARIO-->
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="menu2.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="style_contact.css" type="text/css" media="screen">-->
<SCRIPT language="JavaScript" type="text/javascript">
function limpiarError()
{
    document.getElementById("nombreError").innerHTML="";
    return true;
}
   
   <!--La funci? chequeoFinal permite verificar que los campos obligatorios hayan sido llenados-->
     function chequeoFinal(){
        var control=false;
		if(chequear(document.f.cod_prod, "Codigo del Producto"))
		if(chequear(document.f.nombre, "Nombre Producto"))
		if(chequear(document.f.cantidad, "Cantidad de Articulos"))
        if(chequearNumero(document.f.cantidad, "Cantidad de Articulos"))
		if(chequear(document.f.fecha_caducidad, "Fecha Caducidad"))
        if(chequear(document.f.observacion, "Observaciones"))
		control=true;
        if(control){
            document.getElementById("nombreError").innerHTML="Verificacion de campos completada con exito!!!";        
        }else{
            return false;
        }
		}
	<!--Fin de funci? chequeoFinal-->
	function chequear(k, nomb) {
    <!--Usado para verificar los campos vac?s-->
        if(k.value.length==0){
            document.getElementById("nombreError").innerHTML="Lo siento "+ nomb +" no puede estar vacio";        
			k.focus();
            return false;
        } else {
            return true;
        }
    }
    <!--Fin de funci? chequear-->

    function chequearNumero(k,nomb){
            if(isNaN(k.value)){
            document.getElementById("nombreError").innerHTML="El "+nomb+" debe contener numeros";        
				k.value="";
				k.focus();
                return false;
            } else    {
				return true;
            }
    }
	</SCRIPT>
<style>
    .error  {color:#f00;font-weight:bold;text-align:center}
</style>
</head>
<body onload="document.f.cod_prod.focus();">
	<div id="wrap">
	<!--top part start -->
	<div id="top">
	</div>
	<div id="top1">
	</div>
	
	<!--top part end -->
    <!--body start -->
<div id="body">
	<br class="spacer" />
   <!--left panel end -->
   <!--mid panel start -->
  <div id="mid_admin">
  <div class="fondo_azul">
	<?php include("includes/menu_superior.php");?>
		</div>
		<br />
  	<h3 align="center">Registro de Productos</h3>
		<h2 align="center" style="color:#ff0000;font-size:14px;">Campos obligatorios (*)</h2>
		<br />
		<form id="ContactForm" name="f" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" onSubmit="return chequeoFinal();" onreset="return limpiarError();">
<div id="nombreError" class="error"></div>    
<br></br>
		
			<div>
		<table align="center" border="0">
			<tr>
				<td>Cod. Producto</td><td><INPUT type="text" name="cod_prod"><small style="color:#ff0000;font-size:14px;">*</small></td>
			</tr>
			<tr>
				<td>Nombre</td><td><INPUT type="text" name="nombre" ><small style="color:#ff0000;font-size:14px;">*</small></td>
			</tr>
			<tr>
				<td>Cantidad</td><td><INPUT type="text" name="cantidad" ><small style="color:#ff0000;font-size:14px;">*</small></td>
			</tr>
			<tr>
				<td>Fecha caducidad</td>
				<td><input type="text" class="input" name="fecha_caducidad" id="campo_fecha"><input type="button" id="lanzador" value="..." /><small style="color:#ff0000;font-size:14px;">*</small></td>
			</tr>
			<tr>
				<td>Observaciones</td><td><textarea name="observacion"placeholder="Haga todas las observaciones acerca del material a registrar"></textarea><small style="color:#ff0000;font-size:14px;">*</small></td>
			</tr>
			<tr>
				<td colspan="3"><div style="width:350px;text-align:center;border:1px solid #ffffff;font-weight:bold;"><?php echo $resp_l;?></div></td>
			</tr>
		</table><center>
					<input name="status" type="hidden" value="1"><br />
            	<input type="submit" name="registrar" value="Registrar" class="button"/>
				<input type="reset" name="limpiar" value="Limpiar" class="button" />
				<a href="index_productos.php"><input type="button" value="Regresar"/></a>
            <button type="button" onclick="window.open('ayuda.pdf','_blank')">Ayuda</button>
				</center>
		</div>
	</form>
   <h2><br class="spacer" />
   </h2>
  </div>
   <!--mid panel end -->

	
	<br class="spacer" />
	<br />
	<br />
</div>
<?php include("includes/footer_index.php");?>
</div>
	<!--body end -->
	<script type="text/javascript">
    Calendar.setup({
        inputField     :    "campo_fecha",      // id del campo de texto
        ifFormat       :    "%Y/%m/%d",       // formato de la fecha, cuando se escriba en el campo de texto
        button         :    "lanzador"   // el id del bot�n que lanzar� el calendario
    });
</script>
</body>
</html>