<?php
	if(!isset($_SESSION)){session_start();}
	if($_SESSION['administrador']!="si"){header("Location: index.php");exit;}
	include "ConexBd.php";
	$conn=new ConexBd();
	$db=$conn->db;
	//abrimos conexion
	$idconn=$conn->conectar();
	//seleccionamos la bd
	$conn->seleccionarBd($db,$idconn);
	////////////////////////////////////////////////////////////////////////////
	if(!isset($_SESSION)){session_start();}
	if(isset($_SESSION["cedula"])){$cedulas=$_SESSION["cedula"];}else{$cedula=0;}
	if(isset($_GET['cedula'])){$cedula=$_GET['cedula'];}else{$cedula="";}
	///////////procesar actualizacion///////////////
	// botones del formulario actualizacion
		$enviar = isset($_POST["registrar"]) ? $_POST["registrar"]:"";
		
		if($enviar=="Registrar"){
		
			$cedula=$_POST['cedula'];
		$cod_e=$_POST['cod_e'];
		$fecha_prog=$_POST['fecha_prog'];
		$tipo_repara=$_POST['tipo_repara'];
		$descripcion=$_POST['descripcion'];
		$status=$_POST['status'];
		$fecha=strftime( "%Y-%m-%d", time() );
			
		//creamos la consulta
		$sql="INSERT INTO reparaciones VALUES('','".$cod_e."','".$cedula."','".$status."','".$fecha_prog."','".$fecha."','".$tipo_repara."','".$descripcion."')";
		//usamos la funcion transacciones y la pasamos el string sql y el id de conexion para realizar la operacion que se necesita
		$ins=$conn->transacciones($sql,$idconn);
		$resp_l = "Reparacion o Mantenimiento Registrado con Exito";
			if(!$ins){
				$resp_l = "Operacion Fallida Intentelo de Nuevo";
			}
		}else{
			$resp_l = "Formulario Planificacion de Reparacion y Mantenimiento Equipos";
		}
		
	////////////////////////////////////////////////
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--INICIO INCLUDES DEL CALENDARIO-->

<link rel="stylesheet" type="text/css" media="all" href="calendario/calendar-green.css" title="win2k-cold-1" />
<!-- librer�a principal del calendario -->
  <script type="text/javascript" src="calendario/calendar.js"></script>
<!-- librer�a para cargar el lenguaje deseado -->
  <script type="text/javascript" src="calendario/calendar-es.js"></script>
<!-- librer�a que declara la funci�n Calendar.setup, que ayuda a generar un calendario en unas pocas l�neas de c�digo -->
  <script type="text/javascript" src="calendario/calendar-setup.js"></script>
  
<!--FIN INCLUDES DEL CALENDARIO-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Registro Reparacion/Mantenimiento</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="style_contac.css" type="text/css" media="screen">
<!--Funci? usada para mostrar algunas alertas tan pronto se carga la p?ina-->
<SCRIPT language="JavaScript" type="text/javascript">
function limpiarError()
{
    document.getElementById("nombreError").innerHTML="";
    return true;
}
   
   <!--La funci? chequeoFinal permite verificar que los campos obligatorios hayan sido llenados-->
     function chequeoFinal(){
        var control=false;
		if(chequear(document.f.fecha_prog, "Fecha programacion"))
        if(chequear(document.f.descripcion, "Descripcion"))
		control=true;
        if(control){		
            document.getElementById("nombreError").innerHTML="Verificacion de campos completada con exito!!!";        
        }else{
            return false;
        }
		}
	<!--Fin de funci? chequeoFinal-->
		function chequearemail(em) {
	if(chequear(document.f.email,"Correo Electronico")){
	txt = em.value;		 
	a2 = txt.indexOf("@");
	len=txt.length;
	if (len!= 0){
		if (a2 < 3) {
            document.getElementById("nombreError").innerHTML="Deben haber al menos 3 caracteres antes del @";        
			document.f.email.focus();
			return false;
		} 
		a3 = txt.lastIndexOf(".");
		checklast = len-a3;
		if (checklast < 3) {
            document.getElementById("nombreError").innerHTML="La extension de dominio es invalida!";        
			document.f.email.focus();
			return false;
		}
		middot = txt.indexOf(".",a2);
		len1 = middot-a2;
		if (len1<1) {
            document.getElementById("nombreError").innerHTML="Dominio invalido!";        
			document.f.email.focus();
			return false;
		}
		else {
		return true;
		}
	}  
	else {
		return true;
	}
	}
	}
	function chequear(k, nomb) {
    <!--Usado para verificar los campos vac?s-->
        if(k.value.length==0){
            document.getElementById("nombreError").innerHTML="Lo siento "+ nomb +" no puede estar vacio";        
			k.focus();
            return false;
        } else {
            return true;
        }
    }
	function chequearTel(k, nomb) {
    <!--Usado para verificar los campos vac?s-->
        if((k.value.length>=0 && k.value.length<11) || (k.value.length>11)){
            document.getElementById("nombreError").innerHTML="El Campo "+ nomb +" debe tener 11 digitos Ej. 04148888888";        
			k.focus();
            return false;
        } else {
            return true;
        }
    }
    <!--Fin de funci? chequear-->

    function chequearNumero(k,nomb){
            if(isNaN(k.value)){
            document.getElementById("nombreError").innerHTML="El "+nomb+" debe contener numeros";        
				k.value="";
				k.focus();
                return false;
            } else    {
				return true;
            }
    }
	</SCRIPT>
<style>
    .error  {color:#f00;font-weight:bold;text-align:center}
</style>
</head>
<body>
<div id="wrap">
	<!--top part start -->
	<div id="top">
	</div>
	<div id="top1">
	</div>
	<!--top part end -->
    <!--body start -->
<div id="body"onload="document.f.cedula.focus();">
	<br class="spacer" />
  <br class="spacer" />
  
   <!--left panel end -->
   <!--mid panel start -->
  <div id="mid_admin">
  <div class="fondo_azul">
	<?php include("includes/menu_superior.php");?>
		</div>
		<br />
  <h2 align="center">Programaci&oacute;n Reparaci&oacute;n/Mantenimiento Equipos</span></h2>
  <h2 align="center" style="color:#ff0000;font-size:14px;">Campos obligatorios (*)</h2>
	<br />
    <form name="f" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" onSubmit="return chequeoFinal();" onreset="return limpiarError();">
<div id="nombreError" class="error"></div>    
<br></br>
		<table align="center" border="0">
			<tr>
				<!--<td>Num. C&eacute;dula</td>--><td><INPUT type="hidden" name="cedula" value="<?php echo $cedulas;?>"><!--<small style="color:#ff0000;font-size:14px;">*</small>--></td>
			</tr>
			<tr>
				<td>Equipo</td>
				<td>
					<select name="cod_e">
									<?php
										$consulta = "SELECT * FROM equipos";
										$result=mysql_query($consulta,$idconn);
										if($result<=0){echo"error--->".mysql_error();}
										while ($row=mysql_fetch_array($result))
											{echo '<option value='.$row['cod_e'].'>'.$row['nomb_e'].'</option>';};
										//$conn->desconectar($idconn);	
									?>  
					</select><small style="color:#ff0000;font-size:14px;">*</small>
				</td>
			</tr>
			<tr>
				<td>Fecha Programaci&oacute;n:</td><td><input type="text" class="input" name="fecha_prog" id="campo_fecha" placeholder="Fecha programada para el mantenimiento/reparacion"><input type="button" id="lanzador" value="..." /></td>
			</tr>
			<tr>
				<td>Tipo Reparaci&oacute;n</td>
				<td>
					<select name="tipo_repara"><small style="color:#ff0000;font-size:14px;">*</small>
						<option value="1">Preventivo</option>
						<option value="2">Correctivo</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Descripci&oacute;n</td><td><TEXTAREA name ="descripcion"></TEXTAREA><small style="color:#ff0000;font-size:14px;">*</small></td>
			</tr>
			<tr>
				<td colspan="3"><div style="width:350px;text-align:center;border:1px solid #ffffff;font-weight:bold;"><?php echo $resp_l;?></div></td>
			</tr>
			<tr>
			<INPUT type="hidden" name="status" value="1">
				<td colspan="2" align="center">
				<br>
				<INPUT type="submit" name="registrar"value="Registrar" ><INPUT type="reset" value="Limpiar"/>
				<a href="index_admin.php"><input type="button" value="Regresar"/></a>
            <button type="button" onclick="window.open('ayuda.pdf','_blank')">Ayuda</button>
				<br><br>
				</td>
			</tr>
		</table>
	</form>
    
  </div>
   <!--mid panel end -->
	<br class="spacer" />
	<?php include("includes/footer_index.php"); ?>
</div>
	<!--body end -->
	</div>
	<script type="text/javascript">
    Calendar.setup({
        inputField     :    "campo_fecha",      // id del campo de texto
        ifFormat       :    "%Y/%m/%d",       // formato de la fecha, cuando se escriba en el campo de texto
        button         :    "lanzador"   // el id del bot�n que lanzar� el calendario
    });
</script>
</body>
</html>