<?php
	if(!isset($_SESSION)){session_start();}
	if($_SESSION['administrador']!="si"){header("Location: index.php");exit;}
	if(isset($_GET['cedula'])){$cedula=$_GET['cedula'];}else{$cedula="";}
        $enviar = isset($_POST["registrar"]) ? $_POST["registrar"]:"";
        
        if($enviar=="Registrar"){
            
            include "ConexBd.php";          
            $conn=new ConexBd();
            $db=$conn->db;		
            
            $cedula=$_POST['cedula'];
            $nombre=$_POST['nombre'];
            $apellido=$_POST['apellido'];
            $telefono=$_POST['telefono'];
            $email=$_POST['email'];
            $direccion=$_POST['direccion'];
            $cargo=$_POST['cargo'];
            $departamento=$_POST['depto'];
            $fecha=strftime( "%Y-%m-%d", time() );

            $idconn=$conn->conectar();
            $conn->seleccionarBd($db,$idconn);
            $sql="INSERT INTO usuarios_autoriza VALUES('".$cedula."','".$nombre."','".$apellido."','".$telefono."','".$email."','".$direccion."','".$fecha."','".$cargo."','".$departamento."')";

            $ins=$conn->transacciones($sql,$idconn);
            $resp_l = "Usuario Registrado con Exito";
            if(!$ins){
                    $resp_l = "Operacion Fallida Intentelo de Nuevo";
            }else{
                    $resp_l = "Usuario Registrado con Exito !!!!";
            }
        
        }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Registro Usuario</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="style_contact.css" type="text/css" media="screen">
<!--Funci? usada para mostrar algunas alertas tan pronto se carga la p?ina-->
<SCRIPT language="JavaScript" type="text/javascript">
function limpiarError()
{
    document.getElementById("nombreError").innerHTML="";
    return true;
}
   
   <!--La funci? chequeoFinal permite verificar que los campos obligatorios hayan sido llenados-->
     function chequeoFinal(){
        var control=false;
		if(chequear(document.f.cedula, "Cedula"))
		if(chequearNumero(document.f.cedula, "Cedula"))
        if(chequear(document.f.nombre, "Nombre"))
		if(chequear(document.f.apellido, "Apellido"))
		if(chequearTel(document.f.telefono, "Telefono"))
        if(chequearNumero(document.f.telefono, "Telefono"))
		if(chequearemail(document.f.email))
		if(chequear(document.f.direccion, "direccion"))
		if(chequear(document.f.usuario, "Usuario"))
		if(chequear(document.f.clave, "Clave"))
		control=true;
        if(control){
            document.getElementById("nombreError").innerHTML="Verificacion de campos completada con exito!!!";        
        }else{
            return false;
        }
		}
	<!--Fin de funci? chequeoFinal-->
		function chequearemail(em) {
	if(chequear(document.f.email,"Correo Electronico")){
	txt = em.value;		 
	a2 = txt.indexOf("@");
	len=txt.length;
	if (len!= 0){
		if (a2 < 3) {
            document.getElementById("nombreError").innerHTML="Deben haber al menos 3 caracteres antes del @";        
			document.f.email.focus();
			return false;
		} 
		a3 = txt.lastIndexOf(".");
		checklast = len-a3;
		if (checklast < 3) {
            document.getElementById("nombreError").innerHTML="La extension de dominio es invalida!";        
			document.f.email.focus();
			return false;
		}
		middot = txt.indexOf(".",a2);
		len1 = middot-a2;
		if (len1<1) {
            document.getElementById("nombreError").innerHTML="Dominio invalido!";        
			document.f.email.focus();
			return false;
		}
		else {
		return true;
		}
	}  
	else {
		return true;
	}
	}
	}
	function chequear(k, nomb) {
    <!--Usado para verificar los campos vac?s-->
        if(k.value.length==0){
            document.getElementById("nombreError").innerHTML="Lo siento "+ nomb +" no puede estar vacio";        
			k.focus();
            return false;
        } else {
            return true;
        }
    }
	function chequearTel(k, nomb) {
    <!--Usado para verificar los campos vac?s-->
        if((k.value.length>=0 && k.value.length<11) || (k.value.length>11)){
            document.getElementById("nombreError").innerHTML="El Campo "+ nomb +" debe tener 11 digitos Ej. 04148888888";        
			k.focus();
            return false;
        } else {
            return true;
        }
    }
    <!--Fin de funci? chequear-->

    function chequearNumero(k,nomb){
            if(isNaN(k.value)){
            document.getElementById("nombreError").innerHTML="El "+nomb+" debe contener numeros";        
				k.value="";
				k.focus();
                return false;
            } else    {
				return true;
            }
    }
	</SCRIPT>
<style>
    .error  {color:#f00;font-weight:bold;text-align:center}
</style>
</head>
<body>
<div id="wrap">
	<!--top part start -->
	<div id="top">
	</div>
	<div id="top1">
	</div>
	<!--top part end -->
    <!--body start -->
<div id="body"onload="document.f.cedula.focus();">
	<br class="spacer" />
  <br class="spacer" />
  
   <!--left panel end -->
   <!--mid panel start -->
  <div id="mid_admin">
  <div class="fondo_azul">
	<?php include("includes/menu_superior.php");?>
		</div>
		<br />
  <h2 align="center">Registro de Usuarios Autorizados para Salida de Equipos</span></h2>
  <h2 align="center" style="color:#ff0000;font-size:14px;">Campos obligatorios (*)</h2>
	<br />
    <form name="f" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" onSubmit="return chequeoFinal()" onreset="return limpiarError();">
<div id="nombreError" class="error"></div>    
<br></br>
		<table align="center" border="0">
			<tr>
				<td>Num. C&eacute;dula</td><td><INPUT type="text" name="cedula" ><small style="color:#ff0000;font-size:14px;">*</small></td>
			</tr>
			<tr>
				<td>Nombre</td><td><INPUT type="text" name="nombre"><small style="color:#ff0000;font-size:14px;">*</small></td>
			</tr>
			<tr>
				<td>Apellido</td><td><INPUT type="text" name="apellido"><small style="color:#ff0000;font-size:14px;">*</small></td>
			</tr>
			<tr>
				<td>Tel&eacute;fono</td><td><INPUT type="text" name="telefono"><small style="color:#ff0000;font-size:14px;">*Ej. 04243146655</small></td>
			</tr>
			<tr>
				<td>Email</td><td><INPUT type="text" name="email"><small style="color:#ff0000;font-size:14px;">*Ej. jhondoe@any.com</small></td>
			</tr>
			<tr>
				<td>Direcci&oacute;n</td><td><TEXTAREA name ="direccion"></TEXTAREA><small style="color:#ff0000;font-size:14px;">*</small></td>
			</tr>
			<tr>
				<td>Cargo</td><td>
					<select name="cargo">
						<option value="tsu">TSU Enfermeria</option>
						<option value="licenciado">Lic. Enfermeria</option>
						<option value="auxiliar">Aux. Enfermeria</option>
					</select><small style="color:#ff0000;font-size:14px;">*</small>					
				</td>
			</tr> 
                        <tr>
                            <td colspan="4">Departamento:
                                <select name="depto">
                            <option value="CONSULTA">Consulta</option>
                            <option value="UCI">UCI</option>
                            <option value="TRAUMATOLOGIA">Traumatologia</option>
                            <option value="EMERGENCIA">Emergencia</option>
                            <option value="PEDIATRIA">Pediatria</option>
                            <option value="MATERNIDAD">Maternidad</option>
                            <option value="CIRUGIA">Cirugia</option>
                            <option value="MEDICINA INTERNA">Medicina Interna</option>
                            <option value="QUIROFANO">Quirofano</option>
                            <option value="TOXICOLOGIA">Toxicologia</option>
                            <option value="CENTRAL DE SUMINISTRO">Central de Suministro</option>
                            <option value="HOSPITALIZACION">Hospitalizacion</option>
                            <option value="CIRUGIA PLASTICA">Cirugia Plastica</option>
                                </select>
                            </td>
                        </tr>
			<tr>
				<td colspan="3"><div style="width:350px;text-align:center;border:1px solid #ffffff;font-weight:bold;"><?php echo @$resp_l;?></div></td>
			</tr>
			<tr>
			<INPUT type="hidden" name="status" value="1">
				<td colspan="2" align="center">
				<br>
				<INPUT type="submit"name="registrar" value="Registrar" ><INPUT type="reset" value="Limpiar"/>
				<a href="index_usuarios_autoriza.php"><input type="button" value="Regresar"/></a>
	            <button type="button" onclick="window.open('ayuda.pdf','_blank')">Ayuda</button>
			<br><br>
				</td>
			</tr>
		</table>
	</form>
    
  </div>
   <!--mid panel end -->
	<br class="spacer" />
	<?php include("includes/footer_index.php"); ?>
</div>
	<!--body end -->
	</div>
</body>
</html>