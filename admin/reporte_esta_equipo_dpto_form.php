<?php
    if(!isset($_SESSION)){session_start();}
    if($_SESSION['administrador']!="si"){header("Location: index.php");exit;}
    
    //$cedula=$_SESSION['cedula'];
    
    include "ConexBd.php";
    $conn=new ConexBd();
    $db=$conn->db;
    $idconn=$conn->conectar();
    $conn->seleccionarBd($db,$idconn);

    $panel=true;
    if ( isset($_POST["depto"])) {
        $panel=false;
        $seldpto=$_POST['depto'];
        
	$sql1="SELECT * FROM salidas_e, usuarios WHERE depart_prestamo = '$seldpto' AND salidas_e.cedula=usuarios.cedula AND salidas_e.status='2'";
	$sql2="SELECT * FROM salidas_e, detalle_sal_equipos, equipos WHERE depart_prestamo = '$seldpto' AND salidas_e.id_salida_e=detalle_sal_equipos.id_salida_e AND equipos.cod_e=detalle_sal_equipos.cod_e AND salidas_e.status='2' ORDER BY salidas_e.id_salida_e ASC";
	$ins=$conn->transacciones($sql1,$idconn);
	$ins1=$conn->transacciones($sql2,$idconn);
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css_tabla/bootstrap.min.css" />
<script type="text/JavaScript" src="js_tabla/jquery.min.js"></script>
<script type="text/JavaScript" src="js_tabla/bootstrap.min.js"></script>
<SCRIPT language="JavaScript" type="text/javascript">
function limpiarError()
{
    document.getElementById("nombreError2").innerHTML="";

    return true;
}
</SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Pedidos Equipos</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<style>
    .error  {color:#f00;font-weight:bold;text-align:center}
</style>        
</head>
<body>
<div id="wrap">
    <div id="top">
    </div>
    <div id="top1">
    </div>
    <div id="body">
        <div class="fondo_azul">
        <?php include("includes/menu_superior.php");?>
        </div>
            
        <center><h3><font size='4'><b>Area de Reportes</b></font></h3></center>

        <div class="container">
            <?php echo '<ul id="brochure"class="nav nav-tabs">';
            echo '<li><a data-toggle="tab" href="#seguimientos">Salidas por Departamentos</a></li>';
            echo '</ul>';            
            echo '<div id="seguimientos" class="tab_content fade in active">';            
            ?>        
            <table width="99%" cellpadding="0" cellspacing="0">
                <form name="form1" id="form1" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" onsubmit="return selectedVals();chequeo()" onreset="return limpiarError();">
                    <tr>
                        <td colspan="4">Departamento:
                            <select name="depto">
                                    <option value="uci">UCI</option>
                                    <option value="traumatologia">Traumatologia</option>
                                    <option value="emergencia">Emergencia</option>
                                    <option value="pediatria">Pediatria</option>
                                    <option value="maternidad">Maternidad</option>
                            </select>
                            <samp><input type="submit" id="buscarnro" value="Buscar"/></samp>                        
                        </td>
                    </tr>
                    
                    <h2 align="center">Listado de Equipos Prestados</span></h2>
                    <br />
                    <table align="center" border="1" width="600">
                    <td colspan="7"style="background:#ffffff;"><center><strong>Responsable</strong></center></tr>
                    <th>Nro.</th>
                    <th>Persona que Retira</th>
                    <th>Cedula</th>
                    <th>Departamento</th>
                    <th>Fecha Pedido</th>        
                    <?php
                    //$contador=0;
                    $row=mysql_fetch_array($ins);
                    do{
                    //$contador=$contador+1;
                    ?>       
                        <tr style="background:#ccccc5; text-align:center;">
                        <td><div style="width:20px;"><?php echo $row['id_salida_e'];?></div></td>
                        <td><div style="width:180px;"><?php echo $row['retira'];?></div></td>
                        <td><div style="width:100px;"><?php echo $row['ced_retira'];?></div></td>
                        <td><div style="width:130px;"><?php echo $row['depart_prestamo'];?></div></td>
                        <td><div style="width:100px;"><?php echo $row['fecha_salida'];?></div></td>
                        </tr>
                    <?php
                    }while($row=mysql_fetch_array($ins));
                    ?>
                    <table align="center" border="1" width="600">
                        <tr>
                        <td colspan="7"style="background:#ffffff;"><center><strong>Detalle de los Pedidos</strong></center></tr>
                        </tr>
                        <?php
                        while($row1=mysql_fetch_array($ins1)){
                        ?>
                            <tr style="background:#ccccc5;">
                            <td style="font-weight:bolder;">Num. Pedido:</td>
                            <td><?php echo $row1['id_salida_e'];?></td>
                            <td style="font-weight:bolder;">Equipo:</td>
                            <td><?php echo $row1['nomb_e'];?></td>
                            <td style="font-weight:bolder;">Observacion:</td>
                            <td><?php echo $row1['observacion'];?></td>
                            </tr>
                        <?php } ?>
                    </table>    
                </form>
                <br></br>
            </table>            
            <?php
            echo '</div>';
            ?>        
        </div>
    </div>            
    <?php include("includes/footer_index.php");?>
    </div>
</div>

</body>
</html>