<?php
	if(!isset($_SESSION)){session_start();}
	if($_SESSION['administrador']!="si"){header("Location: index.php");exit;}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--INICIO INCLUDES DEL CALENDARIO-->

<link rel="stylesheet" type="text/css" media="all" href="calendario/calendar-green.css" title="win2k-cold-1" />
<!-- librer�a principal del calendario -->
  <script type="text/javascript" src="calendario/calendar.js"></script>
<!-- librer�a para cargar el lenguaje deseado -->
  <script type="text/javascript" src="calendario/calendar-es.js"></script>
<!-- librer�a que declara la funci�n Calendar.setup, que ayuda a generar un calendario en unas pocas l�neas de c�digo -->
  <script type="text/javascript" src="calendario/calendar-setup.js"></script>
  
<!--FIN INCLUDES DEL CALENDARIO-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Area de Reportes</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="menu2.css" rel="stylesheet" type="text/css" />
<SCRIPT language="JavaScript" type="text/javascript">
function limpiarError()
{
    document.getElementById("nombreError").innerHTML="";
    return true;
}
   
   <!--La funci? chequeoFinal permite verificar que los campos obligatorios hayan sido llenados-->
     function chequeoFinal(){
        var control=false;
        if(chequear(document.f.fecha_inicio, "Fecha Inicio"))
		if(chequear(document.f.fecha_fin, "Fecha Fin"))
		control=true;
        if(control){
            document.getElementById("nombreError").innerHTML="Verificacion de campos completada con exito!!!";        
        }else{
            return false;
        }
		}
	<!--Fin de funci? chequeoFinal-->
		
	function chequear(k, nomb) {
    <!--Usado para verificar los campos vac?s-->
        if(k.value.length==0){
            document.getElementById("nombreError").innerHTML="Lo siento "+ nomb +" no puede estar vacio";        
			k.focus();
            return false;
        } else {
            return true;
        }
    }
    <!--Fin de funci? chequear-->
	</SCRIPT>
<style>
    .error  {color:#f00;font-weight:bold;text-align:center}
</style>
</head>
<body>
	<div id="wrap">
	<!--top part start -->
	<div id="top">
	</div>
	<div id="top1">
	</div>
	<!--top part end -->
    <!--body start -->
<div id="body">
 <!--mid panel start -->
  <div id="mid_admin">
  <!--<div class="fondo_azul">
	<?php //include("includes/menu_superior.php");?>
		</div>--><br />
  <center>
  	<h3>Area de Reportes</h3>
	
		<p style="text-align:center;">Indique el tipo de busqueda a mostrar</p><br />
		    <form id="ContactForm" name="f" action="reporte_reparaf_proceso.php" method="post" onsubmit="return chequeoFinal()" onreset="return limpiarError();">
<div id="nombreError" class="error"></div>    
<br></br>
						<div>
							<!--<div class="wrapper">
								<span>Status Reparaci&oacute;n:</span>
									<select name="tipo_prod">
										<option value="1">Programada</option>
										<option value="2">Realizada</option>
										<option value="3">Cancelada</option>
									</select>
							</div>-->
							<br/>
							<div class="wrapper">
								<span>Fecha Inicio:</span><input type="text" class="input" name="fecha_inicio" id="campo_fecha"><input type="button" id="lanzador" value="..." />
							</div>
							<br />
							<div class="wrapper">
								<span>Fecha Fin:</span><input type="text" class="input" name="fecha_fin" id="campo_fin"><input type="button" id="lanzador1" value="..."/>
							</div>
							<br />
							<input type="submit" value="Mostrar" class="button"/>
							<input type="reset" name="limpiar" value="Limpiar" class="button" />
<td><button type="button" onclick="window.open('ayuda.pdf','_blank')">Ayuda</button></td>
							<!--<a href="#" class="button" onClick="document.getElementById('ContactForm').submit()">Enviar</a>
							<a href="#" class="button" onClick="document.getElementById('ContactForm').reset()">Limpiar</a>-->
						</div>	
					</form>
</center>
   <h2><br class="spacer" />
   </h2>
   <br />
   <?php include("includes/footer_index.php");?>
  </div>

   <!--mid panel end -->
</div>
	<!--body end -->
</div>
<script type="text/javascript">
    Calendar.setup({
        inputField     :    "campo_fecha",      // id del campo de texto
        ifFormat       :    "%Y/%m/%d",       // formato de la fecha, cuando se escriba en el campo de texto
        button         :    "lanzador"   // el id del bot�n que lanzar� el calendario
    });
</script>
<script type="text/javascript">
    Calendar.setup({
        inputField     :    "campo_fin",      // id del campo de texto
        ifFormat       :    "%Y/%m/%d",       // formato de la fecha, cuando se escriba en el campo de texto
        button         :    "lanzador1"   // el id del bot�n que lanzar� el calendario
    });
</script>	
</body>
</html>