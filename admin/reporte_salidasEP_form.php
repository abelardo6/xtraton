<?php
	if(!isset($_SESSION)){session_start();}
	if($_SESSION['administrador']!="si"){header("Location: index.php");exit;}
	if(isset($_GET['val'])){$valor="No hay Informacion para el estatus indicado";}else{$valor="";}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Salidas por Equipo Estatus</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="menu2.css" rel="stylesheet" type="text/css" />
<SCRIPT language="JavaScript" type="text/javascript">
   
   <!--La funci? chequeoFinal permite verificar que los campos obligatorios hayan sido llenados-->
     function chequeoFinal(){
        var control=false;
        if(chequear(document.f.cod_prod, "Codigo Producto"))
		control=true;
        if(control){
		
             alert("Verificacion de campos completada con exito!!!");
        }else{
            return false;
        }
		}
	<!--Fin de funci? chequeoFinal-->
		
	function chequear(k, nomb) {
    <!--Usado para verificar los campos vac?s-->
        if(k.value.length==0){
            alert("Lo siento "+ nomb +" no puede estar vacio");
			k.focus();
            return false;
        } else {
            return true;
        }
    }
    <!--Fin de funci? chequear-->
	</SCRIPT>
</head>
<body>
	<div id="wrap">
	<!--top part start -->
	<div id="top">
	</div>
	<div id="top1">
	</div>
	<!--top part end -->
    <!--body start -->
<div id="body">
 <!--mid panel start -->
  <div id="mid_admin">
  <!--<div class="fondo_azul">
	<?php //include("includes/menu_superior.php");?>
		</div>--><br />
  <center>
  	<h3>Area de Reportes</h3>
	
		<p style="text-align:center;">Aqui mostrara los pedidos de los productos por estatus de operacion<br />Indique el estatus de los pedidos para la busqueda</p><br />
		    <form id="ContactForm" name="f" action="reporte_salidasEP_proceso.php" method="post" onsubmit="return chequeoFinal()">
						<div>
							<div class="wrapper">
								<span>Estatus:</span>
								<select name="status">
									<option value="1">Pendiente</option>
									<option value="2">Entregado</option>
									<option value="3">Cancelado</option>
								</select>
							</div>
							<div>
								<div style="width:350px;text-align:center;border:1px solid #ffffff;font-weight:bold;"><?php echo $valor;?></div>
							</div>
							<br />
							<center>
							<input type="submit" name="cod" value="Mostrar" class="button"/>
							<input type="reset" name="limpiar" value="Limpiar" class="button" />
<td><button type="button" onclick="window.open('ayuda.pdf','_blank')">Ayuda</button></td>
							</center>
						</div>	
					</form>
</center>
<br />
   <h2><br class="spacer" />
   </h2>
   <?php include("includes/footer_index.php");?>
  </div>

   <!--mid panel end -->
</div>
	<!--body end -->
</div>	

</body>
</html>