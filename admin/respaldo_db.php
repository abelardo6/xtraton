<?php
    if(!isset($_SESSION)){session_start();}
    if($_SESSION['administrador']!="si"){header("Location: index.php");exit;}
    
$host="localhost";
$user="root";
$pass="";
$name="ucia";
$tables=false;
$backup_name=false;
        
$link = mysqli_connect($host,$user,$pass,$name);
if (mysqli_connect_errno())   {   echo "Failed to connect to MySQL: " . mysqli_connect_error();   }

mysqli_select_db($link,$name);
mysqli_query($link,"SET NAMES 'utf8'");

if($tables === false)
{
    $tables = array();
    $result = mysqli_query($link,'SHOW TABLES');
    while($row = mysqli_fetch_row($result))
    {
        $tables[] = $row[0];
    }
}
else
{
    $tables = is_array($tables) ? $tables : explode(',',$tables);
}
$return='';
$return .= 'DROP DATABASE IF EXISTS '.$name.';';
$return.= "\n";
$return .= 'CREATE DATABASE '.$name.';';
$return.= "\n";
$return .= 'USE '.$name.';';
$return.= "\n";

foreach($tables as $table)
{
    $result = mysqli_query($link,'SELECT * FROM '.$table);
    $num_fields = mysqli_num_fields($result);

    $row2 = mysqli_fetch_row(mysqli_query($link, 'SHOW CREATE TABLE '.$table));
    $return.= "\n\n".$row2[1].";\n\n";

    for ($i = 0; $i < $num_fields; $i++) 
    {
        $st_counter= 0;
        while($row = mysqli_fetch_row($result))
        {

            if (($table) != 'v_salida_lista' || ($table) != 'v_salida_prod') {
                if ($st_counter%100 == 0 || $st_counter == 0 )  {
                    $return.= "\nINSERT INTO ".$table." VALUES";
                }


                $return.="\n(";
                for($j=0; $j<$num_fields; $j++) 
                {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = str_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                    if ($j<($num_fields-1)) { $return.= ','; }
                }
                $return.=")";
            }    

            if ( ($st_counter+1)%100 == 0  && $st_counter != 0 )    {   $return.= ";";  }
            else                                                {   $return.= ",";  }
            $st_counter = $st_counter +1 ;
        }
        if (substr($return, -1) == ',') {$return = substr($return, 0, -1). ';'; }
    }
    $return.="\n\n\n";
}

$backup_name = $backup_name ? $backup_name : $name."_".date('Y-m-d')."_".date('H-i-s')."_".rand(1,11111111).'.sql';

/* 
 * Ubicar ruta URL completa de tu sitio WEB segun tu maquina o equipo HOST
 * Esto aplica para las dos variables
 * Una se utiliza para la escritura del archivo
 * La otra para ejecutar el download
 */
$folderWrite = 'C:\xampp\htdocs\weboc\Respaldos_db\\';
$folderDown = 'http://localhost/weboc/Respaldos_db/';

// Comandos en linux
//if (!is_dir($folderWrite)) {
//    mkdir($folderWrite, 755, true);
//    chmod($folderWrite, 755);
//}
//$handle2 = file_put_contents($folderWrite.$backup_name,$return);
// Fin de los comandos en linux

// Comandos en Windows
$file = fopen($folderWrite.$backup_name, "w");
fwrite($file, $return.PHP_EOL);
fclose($file);
// Fin de los comandos en linux

$filename = $folderDown.$backup_name;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Administrador</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<!--<link href="menu2.css" rel="stylesheet" type="text/css" />-->

</head>
<body onload="document.f.usuario.focus()">

<div id="wrap">
<!--top part start -->
	<div id="top">
	</div>
<div id="top1">
</div>

<!--top part end -->
<!--body start -->
<div id="body">

	<div id="mid_admin">
	<!--Menu superior-->
	<div class="fondo_azul">
	<?php include("includes/menu_superior.php");?>
		</div>
		<aside style="border:px solid #000000;float:left;width:100%;">
			<center>
			<br/>

	<center><h2>DESCARGUE EL ARCHIVO DE RESPALDO DE LA BASE DE DATOS</h2></center>
			<center><h3><a href="<?php echo $filename; ?>"target="_blank">Descargar Archivo Backup</a></h3></center>

			</center>			</center>
		</aside>
	<center>
           
            <a href="restaurar_db.php"><input type="button" value="Restaurar"/>
            <button type="button" onclick="window.open('ayuda.pdf','_blank')">Ayuda</button>
        </center>
				
		
		<aside style="border:px solid #000000;float:right;width:68%;">
			<table width="100%" align="center">
			</table>
		</aside>
		<br class="spacer" />
		
	</div>
	<?php include("includes/footer_index.php");?>
</div>
	<!--body end -->
	
</div><!--CIERRE WRAP-->

</body>
</html>
