<?php
    if(!isset($_SESSION)){session_start();}
    if($_SESSION['administrador']!="si"){header("Location: index.php");exit;}
    
/* 
 * Ubicar ruta URL completa de tu sitio WEB segun tu maquina o equipo HOST
 * Esto aplica para las dos variables
 * Una se utiliza para la escritura del archivo
 * La otra para ejecutar el download
 */
$folderWrite = 'C:\xampp\htdocs\weboc\Respaldos_db\\';
$folderDown = 'http://localhost/weboc/Respaldos_db/';


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Administrador</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="upview/style.css">
<script src="upview/sorttable.js"></script>
<SCRIPT language="JavaScript" type="text/javascript">
   
   <!--La funci? chequeoFinal permite verificar que los campos obligatorios hayan sido llenados-->
     function chequeoFinal(){
		var r = confirm("Desea eliminar el Registro?");
		if (r == true) {
			
			return true;
		} else {
			return false;
		}	 
		}
</SCRIPT>	

</head>
<body onload="document.f.usuario.focus()">

<div id="wrap">
<!--top part start -->
	<div id="top">
	</div>
<div id="top1">
</div>

<!--top part end -->
<!--body start -->
<div id="body">

	<div id="mid_admin">
	<!--Menu superior-->
	<div class="fondo_azul">
	<?php include("includes/menu_superior.php");?>
		</div>
		<aside style="border:px solid #000000;float:left;width:100%;">
			<center>
			<br/>

			<!--aqui se muestra las variables de sesion creadas-->
	<center><h2>RESTAURAR LA BASE DE DATOS</h2></center>
			<center><h3>Contenido del Directorio</h3></center>
		</aside>
        
<!-- BEGIN -->
		<?php
		if (!empty($_POST['nfile'])) {
    $sql_file = $folderWrite . $_POST['nfile'];
    if (file_exists($sql_file)) {
    $host="localhost";
    $user="root";
    $pass="";
    $dbname="ucia";
        
    $mysqli = new mysqli($host, $user, $pass, $dbname);
    if (mysqli_connect_errno())   {   echo "Falla la conexión a MySQL: " . mysqli_connect_error();   }


    $mysqli->query("SET NAMES 'utf8'");
    $templine = '';
    $lines = file($sql_file);
    foreach ($lines as $line)
    {
        if (substr($line, 0, 2) == '--' || $line == '')
            continue;
        $templine .= $line;
        if (substr(trim($line), -1, 1) == ';')
        {
            $mysqli->query($templine) or print('Error ejecutando el query \'<strong>' . $templine . '\': ' . $mysqli->error . '<br /><br />');
            $templine = '';
        }
    }
    echo '<center><strong><h2>La importacion se ejecuto con Exito!!!.</h2></strong></center>';        
    } else {
        echo '<center><strong> el archivo SQL correctamente!. Archivo: </strong></center>'.$sql_file;        
    }
}
?>        
	<table class="sortable">
<form name="formres" id="formres" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" onsubmit="" onreset="">
	    <thead>
		<tr>
			<th>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Seleccione</th>
			<th>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Nombre del archivo</th>
			<th>&nbsp;  Tipo</th>
			<th></th>
			<!--<th>Size</th>-->
			<!--<th>Date Modified</th>-->
		</tr>
	    </thead>
	    <tbody>

		<?php

	function pretty_filesize($file) {
		$size=filesize($file);
		if($size<1024){$size=$size." Bytes";}
		elseif(($size<1048576)&&($size>1023)){$size=round($size/1024, 1)." KB";}
		elseif(($size<1073741824)&&($size>1048575)){$size=round($size/1048576, 1)." MB";}
		else{$size=round($size/1073741824, 1)." GB";}
		return $size;
	}

	if($_SERVER['QUERY_STRING']=="hidden")
	{$hide="";
	 $ahref="./";
	 $atext="Hide";}
	else
	{$hide=".";
	 $ahref="./?hidden";
	 $atext="Show";}

	 $myDirectory=opendir("Respaldos_db/.");

	while($entryName=readdir($myDirectory)) {
	   $dirArray[]=$entryName;
	}

	closedir($myDirectory);

	$indexCount=count($dirArray);
        
	sort($dirArray);

	for($index=0; $index < $indexCount; $index++) {

	    if(substr("$dirArray[$index]", 0, 1)!=$hide) {

		$favicon="";
		$class="file";

		$name=$dirArray[$index];
		$namehref= $dirArray[$index];
                
		$modtime=""; //date("M j Y g:i A", filemtime($folderDown.$dirArray[$index]));
		$timekey=""; //date("YmdHis", filemtime($folderDown.$dirArray[$index]));

		if(is_dir($dirArray[$index]))
		{
				$extn="&lt;Directory&gt;";
				$size="&lt;Directory&gt;";
				$sizekey="0";
				$class="dir";

				if($name=="."){$name=". (Current Directory)"; $extn="&lt;System Dir&gt;"; $favicon=" style='background-image:url($namehref/.favicon.ico);'";}
				if($name==".."){$name=".. (Parent Directory)"; $extn="&lt;System Dir&gt;";}
		}

		else{
			$extn=pathinfo($dirArray[$index], PATHINFO_EXTENSION);

			switch ($extn){
				case "png": $extn="PNG Image"; break;
				case "jpg": $extn="JPEG Image"; break;
				case "jpeg": $extn="JPEG Image"; break;
				case "svg": $extn="SVG Image"; break;
				case "gif": $extn="GIF Image"; break;
				case "ico": $extn="Windows Icon"; break;

				case "txt": $extn="Text File"; break;
				case "log": $extn="Log File"; break;
				case "htm": $extn="HTML File"; break;
				case "html": $extn="HTML File"; break;
				case "xhtml": $extn="HTML File"; break;
				case "shtml": $extn="HTML File"; break;
				case "php": $extn="PHP Script"; break;
				case "js": $extn="Javascript File"; break;
				case "css": $extn="Stylesheet"; break;

				case "pdf": $extn="PDF Document"; break;
				case "xls": $extn="Spreadsheet"; break;
				case "xlsx": $extn="Spreadsheet"; break;
				case "doc": $extn="Microsoft Word Document"; break;
				case "docx": $extn="Microsoft Word Document"; break;

				case "zip": $extn="ZIP Archive"; break;
				case "htaccess": $extn="Apache Config File"; break;
				case "exe": $extn="Windows Executable"; break;

				default: if($extn!=""){$extn=strtoupper($extn)." File";} else{$extn="Unknown";} break;
			}

				$size=""; //pretty_filesize($dirArray[$index]);
				$sizekey=""; //filesize($dirArray[$index]);
		}

	 echo("
		<tr class='$class'>
            <td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type='radio' name='nfile' value='$name' /></td>
			<td><a href='#' class='name'>$name</a></td>
			<td><a href='#'>$extn</a></td>
    		<td><a href='restaurar_eliminar_archivos.php?fname=$name' onClick='return chequeoFinal()'><img src='images/equis.png' width='40'height='40' title='Eliminar Archivo'/></a></td>

			<!--<td sorttable_customkey='$sizekey'><a href='#'>$size</a></td>-->
			<!--<td sorttable_customkey='$timekey'><a href='#'>$modtime</a></td>-->
		</tr>");
	   }
	}
	?>
	    
		</tbody>

	<center>
            <input type="submit" value="Ejecutar" name="registrare"/>
            <button type="button" onclick="window.open('ayuda.pdf','_blank')">Ayuda</button>
        </center>
		
    </form>
	
	</table>
	
<!-- END -->        
                
	</div>
	<?php include("includes/footer_index.php");?>
</div>
	<!--body end -->
	
</div><!--CIERRE WRAP-->

</body>
</html>
