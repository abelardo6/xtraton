'use strict';

var CONTAINER = document.querySelector('.carousel--container');
var PREV = document.querySelector('.carousel--controls-prev');
var NEXT = document.querySelector('.carousel--controls-next');

function hideCurrentSlide() {
  var ACTIVE_SLIDE = document.querySelector('.carousel--slide-active');
  ACTIVE_SLIDE.style.opacity = 0;
  ACTIVE_SLIDE.classList.remove('carousel--slide-active');
}

function showSlide(el, direction) {
  if (direction == 'previous') {
    if (el !== CONTAINER.firstElementChild) {
      el.previousElementSibling.style.opacity = 1;
      el.previousElementSibling.classList.add('carousel--slide-active');
    } else {
      CONTAINER.lastElementChild.style.opacity = 1;
      CONTAINER.lastElementChild.classList.add('carousel--slide-active');
    }
  } else if (direction == 'next') {
    if (el !== CONTAINER.lastElementChild) {
      console.log(el);
      el.nextElementSibling.style.opacity = 1;
      el.nextElementSibling.classList.add('carousel--slide-active');
    } else {
      CONTAINER.firstElementChild.style.opacity = 1;
      CONTAINER.firstElementChild.classList.add('carousel--slide-active');
    }
  }
}

function previousSlide() {
  var ACTIVE_SLIDE = document.querySelector('.carousel--slide-active');
  hideCurrentSlide();
  showSlide(ACTIVE_SLIDE, 'previous');
}

function nextSlide() {
  var ACTIVE_SLIDE = document.querySelector('.carousel--slide-active');
  hideCurrentSlide();
  showSlide(ACTIVE_SLIDE, 'next');
}

PREV.addEventListener('click', function (e) {
  e.preventDefault();
  previousSlide();
});

NEXT.addEventListener('click', function (e) {
  e.preventDefault();
  nextSlide();
});