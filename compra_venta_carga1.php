<?php
	include "ConexBd.php";
	$conn=new ConexBd();
	$db=$conn->db;
	
		//abrimos conexion
		$idconn=$conn->conectar();
		//seleccionamos la bd
		$conn->seleccionarBd($idconn,$db);
		//busqueda productos
		$sql1="SELECT * FROM vendedores, productos WHERE vendedores.rut_vendedor=productos.rut_vendedor AND productos.estatus_producto=1 LIMIT 28";
		$ins1=$conn->transacciones($idconn,$sql1);
		$row1 = mysqli_fetch_assoc($ins1);
		
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" CONTENT="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css" title="default">
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/style1.css" />
 <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">-->
  <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">-->
<!--css paginacion-->
  <link rel="stylesheet" href="css/normalize.min.css">
<title>Xtratton 21 - Compra Venta</title>
<script>
	<!--lateral 1-->
    var imagenes=new Array(
        ['images/publicidad/hoteles/1.jpg',''],
        ['images/publicidad/hoteles/2.jpg','']
        /*['images/publicidad/hoteles/3.jpg','http://www.lawebdelprogramador.com/pdf/'],
        ['images/publicidad/hoteles/4.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/hoteles/5.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	<!--lateral 2-->
	var imagenes1=new Array(
        ['images/publicidad/hoteles/6.jpg',''],
        ['images/publicidad/hoteles/7.jpg',''],
        ['images/publicidad/hoteles/8.jpg','']
        /*['images/publicidad/hoteles/9.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/hoteles/10.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	<!--lateral 3-->
	var imagenes2=new Array(
        ['images/publicidad/hoteles/11.jpg',''],
        ['images/publicidad/hoteles/12.jpg','']
        /*['images/publicidad/hoteles/13.jpg','http://www.lawebdelprogramador.com/pdf/'],
        ['images/publicidad/hoteles/14.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/hoteles/15.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	<!--lateral 4-->
	var imagenes4=new Array(
        ['images/publicidad/hoteles/21.jpg',''],
        ['images/publicidad/hoteles/22.jpg',''],
        ['images/publicidad/hoteles/23.jpg','']
        /*['images/publicidad/hoteles/24.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/hoteles/25.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	<!--inferior-->
	var imagenes3=new Array(
        ['images/publicidad/hoteles/16.jpg',''],
        ['images/publicidad/hoteles/17.jpg',''],
        ['images/publicidad/hoteles/18.jpg','']
        /*['images/publicidad/hoteles/19.jpg','']
		['images/publicidad/hoteles/20.jpg','']*/
	);

	function rotarImagenes(){
        var index=Math.floor((Math.random()*imagenes.length));
		document.getElementById("imagen").src=imagenes[index][0];
		document.getElementById("link").href=imagenes[index][1];
		
	}
	function rotarImagenes1(){
		var index1=Math.floor((Math.random()*imagenes1.length));
		document.getElementById("imagen1").src=imagenes1[index1][0];
		document.getElementById("link1").href=imagenes1[index1][1];
		
	}
	function rotarImagenes2(){
		var index2=Math.floor((Math.random()*imagenes2.length));
		document.getElementById("imagen2").src=imagenes2[index2][0];
		document.getElementById("link2").href=imagenes2[index2][1];
		
	}
	function rotarImagenes3(){
		var index3=Math.floor((Math.random()*imagenes3.length));
		document.getElementById("imagen3").src=imagenes3[index3][0];
		document.getElementById("link3").href=imagenes3[index3][1];
		
	}
	function rotarImagenes4(){
		var index4=Math.floor((Math.random()*imagenes4.length));
		document.getElementById("imagen4").src=imagenes4[index4][0];
		document.getElementById("link4").href=imagenes4[index4][1];
	}

</script>
<script>
	onload=function()
    {
        // Cargamos una imagen aleatoria
        rotarImagenes();
		rotarImagenes1();
		rotarImagenes2();
		rotarImagenes3();
		rotarImagenes4();
 
        // Indicamos que cada 5 segundos cambie la imagen
        setInterval(rotarImagenes,5000);
		setInterval(rotarImagenes1,4000);
		setInterval(rotarImagenes2,6000);
		setInterval(rotarImagenes3,7000);
		setInterval(rotarImagenes4,8000);
    }
</script>
</head>
<body id="page">

        <div class="marco">
            <div class="cabecera">
				<img src="assets/images/banner.jpg">
				<div class="fila_superior_bandera">
                    <?php include"includes/banderas_superior.php";?>
                </div>
            </div>
            <div class="cuerpo">
                <div class="columna_derecha_banderas">
                    <?php include"includes/banderas_derecho.php";?>
                </div>
				<div class="columna_derecha">
                    <a href="" id="link"><img src="" style="width:100%;height:300px;" id="imagen"/></a>
					<a href="" id="link1"><img src="" style="width:100%;height:300px;" id="imagen1"/></a>
					<a href="" id="link2"><img src="" style="width:100%;height:300px;" id="imagen2"/></a>
					<a href="" id="link3"><img src="" style="width:100%;height:300px;" id="imagen3"/></a>
                </div>
				
                <div class="columna_izquierda">
                    <?php include"includes/banderas_izquierdo.php";?>
                </div>
				<div class="columna_izquierda_bandera">
                    <?php include"includes/menu_lateral_inicio_venta.php";?>
                </div>
                <div class="columna_central">
					<ul class="lb-album">
					<?php
						do{
					?>
							<li>
								<a href="#image-<?php echo $row1['id_producto'];?>">
									<img src="<?php echo $row1['img_producto'];?>" alt="<?php echo $row1['nombre_producto'];?>" class="thumb" width="150"height="150" />
								</a>
								<div class="lb-overlay" id="image-<?php echo $row1['id_producto'];?>">
									<a href="#page" class="lb-close">x Cerrar</a>
									<img src="<?php echo $row1['img_producto'];?>" alt="<?php echo $row1['nombre_producto'];?>" class="full" width="450"height="600"/>
									<div>
										<h3>Microondas <span>porvenir spa</h3>
										<p>Totalemente nuevo sin detalles</p>
									</div>
									
								</div>
							</li>
					<?php
						}while($row1 = mysqli_fetch_assoc($ins1));
					?>
				</ul>	
				</div>
            <div class="pie">
                <a href="" id="link4"><img src="" style="width:100%;height:240px;" id="imagen4"/></a>
            </div>
        </div>
		
		
		<script  src="assets/index.js"></script>
    </body>
</html>