<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" CONTENT="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css" title="default">
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/style1.css" />

 <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">-->
  <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">-->
<!--css paginacion-->
  <link rel="stylesheet" href="css/normalize.min.css">
<title>Solicitud de Publicidad</title>
<script>

    var imagenes=new Array(
        ['images/publicidad/contacto/1.jpg','']
        /*['images/publicidad/contacto/2.jpg',''],
        ['images/publicidad/contacto/3.jpg',''],
        ['images/publicidad/contacto/4.jpg',''],
		['images/publicidad/contacto/5.jpg','']*/
	);
	var imagenes1=new Array(
        ['images/publicidad/contacto/6.jpg','']
        /*['images/publicidad/contacto/7.jpg','http://www.lawebdelprogramador.com/foros/'],
        ['images/publicidad/contacto/8.jpg','http://www.lawebdelprogramador.com/pdf/'],
        ['images/publicidad/contacto/9.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/contacto/10.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	var imagenes2=new Array(
        ['images/publicidad/contacto/11.jpg','']
        /*['images/publicidad/contacto/12.jpg','http://www.lawebdelprogramador.com/foros/'],
        ['images/publicidad/contacto/13.jpg','http://www.lawebdelprogramador.com/pdf/'],
        ['images/publicidad/contacto/14.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/contacto/15.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	var imagenes3=new Array(
        ['images/publicidad/contacto/16.jpg','']
        /*['images/publicidad/contacto/17.jpg','http://www.lawebdelprogramador.com/foros/'],
        ['images/publicidad/contacto/18.jpg','http://www.lawebdelprogramador.com/pdf/'],
        ['images/publicidad/contacto/19.jpg','http://www.lawebdelprogramador.com/utilidades/']
		['images/publicidad/contacto/20.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	<!--lateral 4-->
	var imagenes4=new Array(
        ['images/publicidad/contacto/23.jpg','']
        /*['images/publicidad/contacto/21.jpg',''],
        ['images/publicidad/contacto/22.jpg','']
        ['images/publicidad/contacto/24.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/contacto/25.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	function rotarImagenes(){
        var index=Math.floor((Math.random()*imagenes.length));
		document.getElementById("imagen").src=imagenes[index][0];
		document.getElementById("link").href=imagenes[index][1];
		
	}
	function rotarImagenes1(){
		var index1=Math.floor((Math.random()*imagenes1.length));
		document.getElementById("imagen1").src=imagenes1[index1][0];
		document.getElementById("link1").href=imagenes1[index1][1];
		
	}
	function rotarImagenes2(){
		var index2=Math.floor((Math.random()*imagenes2.length));
		document.getElementById("imagen2").src=imagenes2[index2][0];
		document.getElementById("link2").href=imagenes2[index2][1];
		
	}
	function rotarImagenes3(){
		var index3=Math.floor((Math.random()*imagenes3.length));
		document.getElementById("imagen3").src=imagenes3[index3][0];
		document.getElementById("link3").href=imagenes3[index3][1];
		
	}
	function rotarImagenes4(){
		var index4=Math.floor((Math.random()*imagenes4.length));
		document.getElementById("imagen4").src=imagenes4[index4][0];
		document.getElementById("link4").href=imagenes4[index4][1];
	}
</script>
<script>
	onload=function()
    {
        // Cargamos una imagen aleatoria
        rotarImagenes();
		rotarImagenes1();
		rotarImagenes2();
		rotarImagenes3();
		rotarImagenes4();
 
        // Indicamos que cada 5 segundos cambie la imagen
        setInterval(rotarImagenes,5000);
		setInterval(rotarImagenes1,5000);
		setInterval(rotarImagenes2,5000);
		setInterval(rotarImagenes3,5000);
		setInterval(rotarImagenes4,5000);
    }
</script>
</head>
<body id="page">

        <div class="marco">
            <div class="cabecera">
				<img src="assets/images/banner.jpg">
				<div class="fila_superior_bandera">
                    <?php include"includes/banderas_superior.php";?>
                </div>
            </div>
            <div class="cuerpo">
                <div class="columna_derecha_banderas">
                    <?php include"includes/banderas_derecho.php";?>
                </div>
				<div class="columna_derecha">
                    <a href="" id="link"><img src="" style="width:100%;height:300px;" id="imagen"/></a>
					<a href="" id="link1"><img src="" style="width:100%;height:300px;" id="imagen1"/></a>
					<a href="" id="link2"><img src="" style="width:100%;height:300px;" id="imagen2"/></a>
					<a href="" id="link4"><img src="" style="width:100%;height:300px;" id="imagen4"/></a>
                </div>
				
                <div class="columna_izquierda">
                    <?php include"includes/banderas_izquierdo.php";?>
                </div>
				<div class="columna_izquierda_bandera">
                    <?php include"includes/menu_lateral_inicio_venta.php";?>
                </div>
                <div class="columna_central_padding">
					<h1>Informaci&oacute;n de Contacto</h1>
				<p>Con el af&aacute;n de mantener el contacto con nuestra clientela ponemos a su disposici&oacute;n los siguientes puntos
				de contacto </p><br /><br />
				<p>Departamento de administraci&oacute;n:<span style="color:#707070;">administracion@xtraton21.cl</span> </p>
				<p>Departamento de Ventas: <span style="color:#707070;">ventas@xtraton21.cl</span></p>
				<p>Departamento de Soporte: <span style="color:#707070;">soporte@xtraton21.cl</span></p>
				<p>N&uacute;mero de Contacto: <span style="color:#707070;">+569 50526553</span></p><br/>
				<p>&nbsp; <span style="color:#707070;">+569 57168721</span></p><br/>
				<h3>Horarios de Atenci&oacute;n</h3>
				<p>Lunes a Viernes: <span style="color:#707070;"> de 9:00am a 9:00pm</span></p>
				<p>S&aacute;bado: <span style="color:#707070;">de 9:00am a 6:00pm</span></p>
				<h1>Nuestra Ubicación</h1>
				<iframe src="https://www.google.com/maps/d/embed?mid=1aIPvjxYwVuHJRG1RYkx-rxKZko9wG1Ab" width="640" height="480"></iframe>	
				</div>
            <div class="pie">
                <a href="" id="link3"><img src="" style="width:100%;height:240px;" id="imagen3"/></a>
            </div>
        </div>
		
		
		
    </body>
</html>