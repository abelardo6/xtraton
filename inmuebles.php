<?php
	include "ConexBd.php";
	session_start();//para carro de compra
	include_once("includes/config.php");//para carro de compra
	$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	$conn=new ConexBd();
	$db=$conn->db;
	$url = "inmuebles.php";
		//abrimos conexion
		$idconn=$conn->conectar();
		//seleccionamos la bd
		$conn->seleccionarBd($idconn,$db);
		//busqueda productos
		$sql1="SELECT * FROM vendedores, productos WHERE vendedores.rut_vendedor=productos.rut_vendedor AND productos.estatus_producto=1";
		$ins1=$conn->transacciones($idconn,$sql1);
		$row1 = mysqli_fetch_assoc($ins1);
		$num_total_registros= mysqli_num_rows($ins1);
///////////////////PAGINACION////////////////////////////////////////////////////////////////
		if ($num_total_registros > 0) {
	//Limito la busqueda
	$TAMANO_PAGINA = 13;
        $pagina = false;

	//examino la pagina a mostrar y el inicio del registro a mostrar
        if (isset($_GET["pagina"]))
            $pagina = $_GET["pagina"];
			
        
	if (!$pagina) {
		$inicio = 0;
		$pagina = 1;
		
	}
	else {
		$inicio = ($pagina - 1) * $TAMANO_PAGINA;
	}
	
	//calculo el total de paginas
	$total_paginas1 = ceil($num_total_registros / $TAMANO_PAGINA);
	$total_paginas = $total_paginas1 - 2;

	$consulta = "SELECT * FROM productos WHERE estatus_producto=1 AND tipo_producto= 'inmueble' LIMIT ".$inicio."," . $TAMANO_PAGINA;
	$rs=$conn->transacciones($idconn,$consulta);
	$row2 = mysqli_fetch_assoc($rs);

	
}
////////////////////////////////////////////////FIN PAGINACION////////////////////////////////////////////////////		
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" CONTENT="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css" title="default">
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/style1.css" />
<link rel="stylesheet" type="text/css" href="css/style_cart.css" />
 <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">-->
  <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">-->

<title>Xtratton 21 - Compra Venta</title>
<script>
	<!--lateral 1-->
    var imagenes=new Array(
        ['images/publicidad/hoteles/1.jpg',''],
        ['images/publicidad/hoteles/2.jpg','']
        /*['images/publicidad/hoteles/3.jpg','http://www.lawebdelprogramador.com/pdf/'],
        ['images/publicidad/hoteles/4.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/hoteles/5.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	<!--lateral 2-->
	var imagenes1=new Array(
        ['images/publicidad/hoteles/6.jpg',''],
        ['images/publicidad/hoteles/7.jpg',''],
        ['images/publicidad/hoteles/8.jpg','']
        /*['images/publicidad/hoteles/9.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/hoteles/10.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	<!--lateral 3-->
	var imagenes2=new Array(
        ['images/publicidad/hoteles/11.jpg',''],
        ['images/publicidad/hoteles/12.jpg','']
        /*['images/publicidad/hoteles/13.jpg','http://www.lawebdelprogramador.com/pdf/'],
        ['images/publicidad/hoteles/14.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/hoteles/15.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	<!--lateral 4-->
	var imagenes4=new Array(
        ['images/publicidad/hoteles/21.jpg',''],
        ['images/publicidad/hoteles/22.jpg',''],
        ['images/publicidad/hoteles/23.jpg','']
        /*['images/publicidad/hoteles/24.jpg','http://www.lawebdelprogramador.com/utilidades/'],
		['images/publicidad/hoteles/25.jpg','http://www.lawebdelprogramador.com/utilidades/']*/
	);
	<!--inferior-->
	var imagenes3=new Array(
        ['images/publicidad/hoteles/16.jpg',''],
        ['images/publicidad/hoteles/17.jpg',''],
        ['images/publicidad/hoteles/18.jpg','']
        /*['images/publicidad/hoteles/19.jpg','']
		['images/publicidad/hoteles/20.jpg','']*/
	);

	function rotarImagenes(){
        var index=Math.floor((Math.random()*imagenes.length));
		document.getElementById("imagen").src=imagenes[index][0];
		document.getElementById("link").href=imagenes[index][1];
		
	}
	function rotarImagenes1(){
		var index1=Math.floor((Math.random()*imagenes1.length));
		document.getElementById("imagen1").src=imagenes1[index1][0];
		document.getElementById("link1").href=imagenes1[index1][1];
		
	}
	function rotarImagenes2(){
		var index2=Math.floor((Math.random()*imagenes2.length));
		document.getElementById("imagen2").src=imagenes2[index2][0];
		document.getElementById("link2").href=imagenes2[index2][1];
		
	}
	function rotarImagenes3(){
		var index3=Math.floor((Math.random()*imagenes3.length));
		document.getElementById("imagen3").src=imagenes3[index3][0];
		document.getElementById("link3").href=imagenes3[index3][1];
		
	}
	function rotarImagenes4(){
		var index4=Math.floor((Math.random()*imagenes4.length));
		document.getElementById("imagen4").src=imagenes4[index4][0];
		document.getElementById("link4").href=imagenes4[index4][1];
	}

</script>
<script>
	onload=function()
    {
        // Cargamos una imagen aleatoria
        rotarImagenes();
		rotarImagenes1();
		rotarImagenes2();
		rotarImagenes3();
		rotarImagenes4();
 
        // Indicamos que cada 5 segundos cambie la imagen
        setInterval(rotarImagenes,5000);
		setInterval(rotarImagenes1,4000);
		setInterval(rotarImagenes2,6000);
		setInterval(rotarImagenes3,7000);
		setInterval(rotarImagenes4,8000);
    }
</script>
<style>
	#compra p:hover{
		background:#cdcdcd;
		color:#fff;
		font-weight:bolder;
	}
	#compra:hover{
		background:#cdcdcd;
		
	}
</style>
<head>
<body id="page">
<!--PARA CARRO DE COMPRA-->
<?php
if(isset($_SESSION["cart_products"]) && count($_SESSION["cart_products"])>0)
{
	echo '<div class="cart-view-table-front" id="view-cart">';
	echo '<h3>Su Carro de Compra</h3>';
	echo '<form method="post" action="includes/cart_update.php">';
	echo '<table width="100%"  cellpadding="6" cellspacing="0">';
	echo '<tbody>';

	$total =0;
	$b = 0;
	foreach ($_SESSION["cart_products"] as $cart_itm)
	{
		$product_name = $cart_itm["nombre_producto"];
		$product_qty = $cart_itm["product_qty"];
		$product_price = $cart_itm["precio_producto"];
		$product_code = $cart_itm["product_code"];
		$bg_color = ($b++%2==1) ? 'odd' : 'even'; //zebra stripe
		echo '<tr class="'.$bg_color.'">';
		echo '<td>Cant. <input type="text" size="2" maxlength="2" name="product_qty['.$product_code.']" value="'.$product_qty.'" /></td>';
		echo '<td>'.$product_name.'</td>';
		echo '<td><input type="checkbox" name="remove_code[]" value="'.$product_code.'" /> Quitar</td>';
		echo '</tr>';
		$subtotal = ($product_price * $product_qty);
		$total = ($total + $subtotal);
	}
	echo '<td colspan="4">';
	echo '<button type="submit">Actualizar</button><a href="includes/view_cart.php" class="button">Ver carro</a>';
	echo '</td>';
	echo '</tbody>';
	echo '</table>';
	
	$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
	echo '</form>';
	echo '</div>';

}
?>
<!--PARA CARRO DE COMPRA-->
        <div class="marco">
            <div class="cabecera">
				<img src="assets/images/banner.jpg">
				<div class="fila_superior_bandera">
                    <?php include"includes/banderas_superior.php";?>
                </div>
            </div>
            <div class="cuerpo">
                <div class="columna_derecha_banderas">
                    <?php include"includes/banderas_derecho.php";?>
                </div>
				<div class="columna_derecha">
                    <a href="" id="link"><img src="" style="width:100%;height:300px;" id="imagen"/></a>
					<a href="" id="link1"><img src="" style="width:100%;height:300px;" id="imagen1"/></a>
					<a href="" id="link2"><img src="" style="width:100%;height:300px;" id="imagen2"/></a>
					<a href="" id="link3"><img src="" style="width:100%;height:300px;" id="imagen3"/></a>
                </div>
				
                <div class="columna_izquierda">
                    <?php include"includes/banderas_izquierdo.php";?>
                </div>
				<div class="columna_izquierda_bandera">
                    <?php include"includes/menu_lateral_inicio_venta.php";?>
                </div>



  	<link rel='stylesheet prefetch' href='plugin/fancybox/css/ryxren.css'>
	<!--<link rel='stylesheet prefetch' href='plugin/fancybox/css/eezerq.css'>-->
	<link rel="stylesheet" href="plugin/fancybox/css/style.css">

  


 







                <div class="columna_central">
					
					
					<?php
						if($rs){ 
						
$products_item = '<ul class="products">';
//fetch results set as object and output HTML
$c=0;
while($obj = $rs->fetch_object())
{
$nombre =  substr($obj->nombre_producto, 0, 10);
$products_item .= <<<EOT
<li class="product">
		<form method="post" action="includes/cart_update.php">
	<div class="product-content"><h3>{$nombre}</h3>
		  <a href="{$obj->img_producto}" data-fancybox="quick-view-{$c}" data-type="image">
		    <img src="{$obj->img_producto}" width="135"height="135"/>
		  </a>
		  <div class="product-form">
		    <h3>{$obj->nombre_producto}</h3><hr>
		    <p>{$obj->descripcion_producto}</p>
		    <p style="margin-top:10px;"><button class="btn" data-fancybox-close>{$currency}{$obj->precio_producto}</button></p>
		  </div>
	<div class="product-info">
	Precio {$currency}{$obj->precio_producto} 
	
	<input type="hidden" name="product_qty" value="1" />
	<input type="hidden" name="product_code" value="{$obj->id_producto}" />
	<input type="hidden" name="type" value="add" />
	<input type="hidden" name="return_url" value="{$current_url}" />

	<div align="center"><button type="submit" class="add_to_cart">Agregar al Carrito</button></div>
	</div>

	</div>
		</form>
	</li>

EOT;
$c++;
}
$products_item .= '</ul>';
echo $products_item;
}
?>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src='plugin/fancybox/js/ryxren.js'></script>
<script  src="plugin/fancybox/js/index.js"></script>
				<table style="width:100%;">
					<tr>
						<td style="left:50%;">
				<?php
				
				echo '<p>';

					if ($total_paginas > 1) {
						if ($pagina != 1)
							echo '<a href="'.$url.'?pagina='.($pagina-1).'"><img src="images/izq.gif" border="0"></a>';
						for ($i=1;$i<=$total_paginas;$i++) {
							if ($pagina == $i)
								//si muestro el �ndice de la p�gina actual, no coloco enlace
								echo $pagina;
							else
								//si el �ndice no corresponde con la p�gina mostrada actualmente,
								//coloco el enlace para ir a esa p�gina
								echo '  <a href="'.$url.'?pagina='.$i.'">'.$i.'</a>  ';
						}
						if ($pagina != $total_paginas)
							echo '<a href="'.$url.'?pagina='.($pagina+1).'"><img src="images/der.gif" border="0"></a>';
					}
					echo '</p>';
				?>
				</td>
				</tr>
				</table>
				</div>
            <div class="pie">
                <a href="" id="link4"><img src="" style="width:100%;height:240px;" id="imagen4"/></a>
            </div>
        </div>
		
		
		<script  src="assets/index.js"></script>
		<script src='js/jquery.min.js'></script>
    </body>
</html>